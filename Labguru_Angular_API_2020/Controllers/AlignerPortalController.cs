﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Labguru_Angular_API_2020.DbContexts;
using Labguru_Angular_DTO;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Options;
using Labguru_Angular_API_2020.Models;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.IO;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Labguru_Angular_API_2020.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AlignerPortalController : ControllerBase
    {
        private readonly SqlDbContext _context;



        public AlignerPortalController(SqlDbContext context)
        {
            _context = context;

        }

        [Route("[action]")]
        [HttpPost]
        public IActionResult GetCommonList(CommonList_Input objCommonInput)
        {
            try
            {
                string input_json =
                        "{Data:" + Newtonsoft.Json.JsonConvert.SerializeObject(objCommonInput) + "}";
                JToken json_Convert = JObject.Parse(input_json);
                var jsonData = new SqlParameter("@jsonData", Convert.ToString(json_Convert));

                SqlDbContext _context_DDL_Master = new SqlDbContext();
                var Data_DDL_List = _context_DDL_Master.DDLMaster.FromSqlRaw<DDLMaster>("exec USP_SELECT_COMMON_LIST_Json_2020 @jsonData", jsonData).ToList();
                JToken json = JObject.Parse(Data_DDL_List[0].DDLData);

                Output_Data output = new Output_Data();
                output.message = "success";
                output.status = 200;
                output.data = json;

                return Ok(output);


            }
            catch (Exception ex)
            {

                Output_Data output = new Output_Data();
                output.message = ex.Message;
                output.status = 409;
                output.data = "";

                return Ok(output);
            }
        }

        [HttpPost]
        [RequestFormLimits(MultipartBodyLengthLimit = 10000000000)]
        [DisableRequestSizeLimit]
        [Consumes("multipart/form-data")]
        [Route("[action]")]
        public IActionResult SubmitCase_Insert(IFormCollection fc)
        {

            try
            {

                SubmitCase_Insert objAligner_Basic_Information = new SubmitCase_Insert();

                objAligner_Basic_Information.objAligner_Basic_Information = (Aligner_Basic_Information)Newtonsoft.Json.JsonConvert.DeserializeObject(fc["objAligner_Basic_Information"], typeof(Aligner_Basic_Information));

                SqlDbContext _context_SubmitCase_Insert = new SqlDbContext();
                var Data = Newtonsoft.Json.JsonConvert.SerializeObject(objAligner_Basic_Information);

                string input_json = Data;
                JToken json_Convert = JObject.Parse(input_json);

                var jsonData = new SqlParameter("@ijsondata", Convert.ToString(json_Convert));
                for (int i = 0; i < fc.Files.Count; i++)
                {
                    string CurrentMonth = DateTime.Now.ToString("MMMM");
                    string CurrentYear = DateTime.Now.ToString("yyyy");

                    SqlDbContext _context_Document = new SqlDbContext();
                    var FolderId = new SqlParameter("@FolderId", 97); // User_Profile_2020 = 98
                    var Document = _context_Document.Document_FolderPath.FromSqlRaw<Document_FolderPath>("exec usp_select_Document_FolderPath @FolderId", FolderId).ToList();
                    string PhysicalPath = Document[0].PhysicalFilePath + json_Convert["objAligner_Basic_Information"]["FirstName"] + "/";

                    string path = PhysicalPath  + "Aligner" + "_" + (i + 1).ToString() + "." + fc.Files[i].FileName.Substring(fc.Files[i].FileName.LastIndexOf('.') + 1); ;

                    if (!System.IO.Directory.Exists(PhysicalPath))
                    {
                        System.IO.Directory.CreateDirectory(PhysicalPath);
                    }

                    var stream = new FileStream(path, FileMode.Create);
                    fc.Files[i].CopyTo(stream);
                    stream.Close();



                }
                SqlDbContext _context_Insert_Update_Output = new SqlDbContext();
                var Result = _context_Insert_Update_Output.AlignerRX_Insert_Update_Output.FromSqlRaw<AlignerRX_Insert_Update_Output>("exec usp_Save_SA_TRxForms_2020 @ijsondata ", jsonData).ToList();



                Output_Data output = new Output_Data();
                output.message = Result[0].message;
                output.status = Result[0].status;
                output.data = Result[0].message;

                return Ok(output);

            }
            catch (Exception ex)
            {

                Output_Data output = new Output_Data();
                output.message = ex.Message;
                output.status = 409;
                output.data = "";
                return Ok(output);
            }
        }

        

        [Route("[action]")]
        [HttpPost]
        public IActionResult GetRXFormDetails(GetRXFormID objCommonInput)
        {
            try
            {
                string input_json =
                        "{objJobEntry_Basic_Information:" + Newtonsoft.Json.JsonConvert.SerializeObject(objCommonInput) + "}";
                JToken json_Convert = JObject.Parse(input_json);
                var jsonData = new SqlParameter("@ijsondata", Convert.ToString(json_Convert));

                SqlDbContext _context_AlignerRX_Get = new SqlDbContext();
                var Data_DDL_List = _context_AlignerRX_Get.AlignerRXForm_Data.FromSqlRaw<AlignerRXForm_Data>("exec usp_select_SA_TRxForms_2020 @ijsondata", jsonData).ToList();
                JToken json = JObject.Parse(Data_DDL_List[0].Result);
               
                Output_Data output = new Output_Data();
                output.message = "success";
                output.status = 200;
                output.data = json;

                return Ok(output);


            }
            catch (Exception ex)
            {

                    Output_Data output = new Output_Data();
                output.message = ex.Message;
                output.status = 409;
                output.data = "";

                return Ok(output);
            }
        }

        
        [Route("[action]")]
        [HttpPost]
        public IActionResult GetDashboardStatus()
        {
            try
            {
               

                SqlDbContext _context_AlignerRX_Dashboard = new SqlDbContext();
                var Data_DDL_List = _context_AlignerRX_Dashboard.AlignerDashboard_status.FromSqlRaw<AlignerDashboard_status>("exec usp_GetDashBoard_RxStatusWiseCount").ToList();
                string Data =
                     "{Data:" + Newtonsoft.Json.JsonConvert.SerializeObject(Data_DDL_List) + "}";
                JToken json = JObject.Parse(Data);

                Output_Data output = new Output_Data();
                output.message = "success";
                output.status = 200;
                output.data = json;

                return Ok(output);


            }
            catch (Exception ex)
            {

                    Output_Data output = new Output_Data();
                output.message = ex.Message;
                output.status = 409;
                output.data = "";

                return Ok(output);
            }
        }
    }
}
