﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Labguru_Angular_API_2020.DbContexts;
using Labguru_Angular_DTO;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Options;
using Labguru_Angular_API_2020.Models;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using System.Net.Http;

namespace Labguru_Angular_API_2020.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommonController : ControllerBase
    {
        private readonly SqlDbContext _context;



        public CommonController(SqlDbContext context)
        {
            _context = context;

        }

        [Route("[action]")]
        [HttpPost]
        public IActionResult GetCommonList(CommonList_Input objCommonInput)
        {
            try
            {
                string input_json =
                        "{Data:" + Newtonsoft.Json.JsonConvert.SerializeObject(objCommonInput) + "}";
                JToken json_Convert = JObject.Parse(input_json);
                var jsonData = new SqlParameter("@jsonData", Convert.ToString(json_Convert));

                SqlDbContext _context_DDL_Master = new SqlDbContext();
                var Data_DDL_List = _context_DDL_Master.DDLMaster.FromSqlRaw<DDLMaster>("exec USP_SELECT_COMMON_LIST_Json_2020 @jsonData", jsonData).ToList();


                //SqlDbContext _context_RxTpe = new SqlDbContext();
                //var SituationID_RxType = new SqlParameter("@SituationID", 1); //-01.Rx Type 
                //var Data_RxType = _context_RxTpe.Common.FromSqlRaw<CommonOutPut>("exec USP_SELECT_COMMON_LIST_2020 @jsonData, @SituationID", jsonData, SituationID_RxType).ToList();


                //SqlDbContext _context_OU = new SqlDbContext();
                //var SituationID_OU = new SqlParameter("@SituationID", 2); //02. OU
                //var Data_OU = _context_OU.Common.FromSqlRaw<CommonOutPut>("exec USP_SELECT_COMMON_LIST_2020 @jsonData,@SituationID", jsonData, SituationID_OU).ToList();


                //SqlDbContext _context_Location = new SqlDbContext();
                //var SituationID_Location = new SqlParameter("@SituationID", 3); //03. Location
                //var Data_Location = _context_Location.Common.FromSqlRaw<CommonOutPut>("exec USP_SELECT_COMMON_LIST_2020 @jsonData,@SituationID", jsonData, SituationID_Location).ToList();



                //SqlDbContext _context_Job_Type = new SqlDbContext();
                //var SituationID_Job_Type = new SqlParameter("@SituationID", 5); //05. Job Type
                //var Data_Job_Type = _context_Job_Type.Common.FromSqlRaw<CommonOutPut>("exec USP_SELECT_COMMON_LIST_2020 @jsonData,@SituationID", jsonData, SituationID_Job_Type).ToList();


                //SqlDbContext _context_Case_Type = new SqlDbContext();
                //var SituationID_Case_Type = new SqlParameter("@SituationID", 6); //06. Case Type
                //var Data_Case_Type = _context_Case_Type.Common.FromSqlRaw<CommonOutPut>("exec USP_SELECT_COMMON_LIST_2020 @jsonData,@SituationID", jsonData, SituationID_Case_Type).ToList();

                //SqlDbContext _context_Digital_Dentist = new SqlDbContext();
                //var SituationID_Digital_Dentist = new SqlParameter("@SituationID", 7); //07. Digital Dentist
                //var Data_Digital_Dentist = _context_Digital_Dentist.Common.FromSqlRaw<CommonOutPut>("exec USP_SELECT_COMMON_LIST_2020 @jsonData,@SituationID", jsonData, SituationID_Digital_Dentist).ToList();

                //SqlDbContext _context_No_warranty = new SqlDbContext();
                //var SituationID_No_warranty = new SqlParameter("@SituationID", 8); //08. No warranty
                //var Data_No_warranty = _context_No_warranty.Common.FromSqlRaw<CommonOutPut>("exec USP_SELECT_COMMON_LIST_2020 @jsonData,@SituationID", jsonData, SituationID_No_warranty).ToList();


                //SqlDbContext _context_Delivery_Boy = new SqlDbContext();
                //var SituationID_Delivery_Boy = new SqlParameter("@SituationID", 9); //09. Delivery Boy
                //var Data_Delivery_Boy = _context_Delivery_Boy.Common.FromSqlRaw<CommonOutPut>("exec USP_SELECT_COMMON_LIST_2020 @jsonData,@SituationID", jsonData, SituationID_Delivery_Boy).ToList();

                //SqlDbContext _context_Thirth_Party_Lab = new SqlDbContext();
                //var SituationID_Thirth_Party_Lab = new SqlParameter("@SituationID", 10); //10. Thirth Party Lab
                //var Data_Thirth_Party_Lab = _context_Thirth_Party_Lab.Common.FromSqlRaw<CommonOutPut>("exec USP_SELECT_COMMON_LIST_2020 @jsonData,@SituationID", jsonData, SituationID_Thirth_Party_Lab).ToList();

                //SqlDbContext _context_Design = new SqlDbContext();
                //var SituationID_Design = new SqlParameter("@SituationID", 11); //11. Design
                //var Data_Design = _context_Design.Common.FromSqlRaw<CommonOutPut>("exec USP_SELECT_COMMON_LIST_2020 @jsonData,@SituationID", jsonData, SituationID_Design).ToList();

                //SqlDbContext _context_Urgent_Case = new SqlDbContext();
                //var SituationID_Urgent_Case = new SqlParameter("@SituationID", 12); //12.  Urgent Case
                //var Data_Urgent_Case = _context_Urgent_Case.Common.FromSqlRaw<CommonOutPut>("exec USP_SELECT_COMMON_LIST_2020 @jsonData,@SituationID", jsonData, SituationID_Urgent_Case).ToList();



                //SqlDbContext _context_Bad_Imp_Reason = new SqlDbContext();
                //var SituationID_Bad_Imp_Reason = new SqlParameter("@SituationID", 22); //22. Bad Imp Reason
                //var Data_Bad_Imp_Reason = _context_Bad_Imp_Reason.Common.FromSqlRaw<CommonOutPut>("exec USP_SELECT_COMMON_LIST_2020 @jsonData, @SituationID", jsonData, SituationID_Bad_Imp_Reason).ToList();


                //if (Data_RxType.Count > 0)
                //{


                //string Data =
                //        "{RxType:" + Newtonsoft.Json.JsonConvert.SerializeObject(Data_RxType) + "," +
                //        "OU:" + Newtonsoft.Json.JsonConvert.SerializeObject(Data_OU) + "," +
                //        "Location:" + Newtonsoft.Json.JsonConvert.SerializeObject(Data_Location) + "," +
                //        //  "Numbering_Type:" + Newtonsoft.Json.JsonConvert.SerializeObject(Data_Numbering_Type) + "," +
                //        "Job_Type:" + Newtonsoft.Json.JsonConvert.SerializeObject(Data_Job_Type) + "," +
                //        "Bad_Imp_Reason:" + Newtonsoft.Json.JsonConvert.SerializeObject(Data_Bad_Imp_Reason) + "," +
                //        "Case_Type:" + Newtonsoft.Json.JsonConvert.SerializeObject(Data_Case_Type) + "," +
                //        "Digital_Dentist:" + Newtonsoft.Json.JsonConvert.SerializeObject(Data_Digital_Dentist) + "," +
                //        "No_warranty:" + Newtonsoft.Json.JsonConvert.SerializeObject(Data_No_warranty) + "," +
                //        "Delivery_Boy:" + Newtonsoft.Json.JsonConvert.SerializeObject(Data_Delivery_Boy) + "," +
                //        "Thirth_Party_Lab:" + Newtonsoft.Json.JsonConvert.SerializeObject(Data_Thirth_Party_Lab) + "," +

                //        "Design:" + Newtonsoft.Json.JsonConvert.SerializeObject(Data_Design) + "," +
                //        "Urgent_Case:" + Newtonsoft.Json.JsonConvert.SerializeObject(Data_Urgent_Case) + "}";





                JToken json = JObject.Parse(Data_DDL_List[0].DDLData);

                Output_Data output = new Output_Data();
                output.message = "success";
                output.status = 200;
                output.data = json;

                return Ok(output);


            }
            catch (Exception ex)
            {

                Output_Data output = new Output_Data();
                output.message = ex.Message;
                output.status = 409;
                output.data = "";

                return Ok(output);
            }
        }


        [Route("[action]")]
        [HttpPost]
        public IActionResult Get_Numbering_Type(CommonList_NS_Input objCommonList_NS_Input)
        {
            try
            {



                SqlDbContext _context_Numbering_Type = new SqlDbContext();
                var LoginCompanyID = new SqlParameter("@LoginCompanyID", objCommonList_NS_Input.LoginCompanyID);
                var OrganizationUnitID = new SqlParameter("@OrganizationUnitID", objCommonList_NS_Input.OrganizationUnitID);
                var SSCID = new SqlParameter("@SSCID", objCommonList_NS_Input.SSCID);
                var Data_Numbering_Type = _context_Numbering_Type.Common.FromSqlRaw<CommonOutPut>("exec USP_SELECT_COMMON_LIST_NS_2020 @LoginCompanyID,@OrganizationUnitID,@SSCID", LoginCompanyID, OrganizationUnitID, SSCID).ToList();


                string Data =
                        "{Numbering_Type:" + Newtonsoft.Json.JsonConvert.SerializeObject(Data_Numbering_Type) + "}";





                JToken json = JObject.Parse(Data);

                Output_Data output = new Output_Data();
                output.message = "success";
                output.status = 200;
                output.data = json;

                return Ok(output);




                // string strJSONObject = "{'Data':{" + Newtonsoft.Json.JsonConvert.SerializeObject(Data_RxType) + " }}";

                //   string strJSONObject = "{'RxType':{" + Newtonsoft.Json.JsonConvert.SerializeObject(Data_RxType) + "  }}";

                // Regex.Unescape(Data);
                //   Data.Replace(@"\", "");
                // Regex.Unescape(Data);
                //  Regex.Unescape(output.data);

                // JObject.Parse(Data);
                //output.data.Replace("\\", "");
                //return Ok(output);
                //}
                //else
                //{
                //    return Data;// NotFound();
                //}


                //string strJSONObject = "{'jsonOfObject':{" + Data + " }}";
                //JToken json = JObject.Parse(strJSONObject);

                //  HttpContent inputContent = new JsonContent(json);



                //   return Ok(json);




            }
            catch (Exception ex)
            {

                Output_Data output = new Output_Data();
                output.message = ex.Message;
                output.status = 409;
                output.data = "";

                return Ok(output);
            }
        }


        [Route("[action]")]
        [HttpPost]
        public IActionResult GetCommonValidation(CommonValidationInput objCommonValidationInPut)
        {

            var SituationID = new SqlParameter("@SituationID", objCommonValidationInPut.SituationID);  //Impression No validation
            var UserID = new SqlParameter("@UserID", objCommonValidationInPut.UserID);
            var ImpressionNo = new SqlParameter("@ImpressionNo", objCommonValidationInPut.ImpressionNo);
            var TransactionNo = new SqlParameter("@TransactionNo", objCommonValidationInPut.TransactionNo);
            var StatusID = new SqlParameter("@StatusID", objCommonValidationInPut.StatusID);
            var ProductID = new SqlParameter("@ProductID", objCommonValidationInPut.ProductID);
            SqlDbContext _context = new SqlDbContext();

            if (TransactionNo.Value == null)
            {
                TransactionNo.Value = "";
            }

            if (ImpressionNo.Value == null)
            {
                ImpressionNo.Value = 0;
            }
            if (ProductID.Value == null)
            {
                ProductID.Value = 0;
            }

            var Data1 = _context.CommonValidationOutput.FromSqlRaw("exec usp_Select_Common_Validation @SituationID ,@UserID,@ImpressionNo,@TransactionNo,@StatusID,@ProductID", SituationID, UserID, ImpressionNo, TransactionNo, StatusID, ProductID).ToList();
            _context.Dispose();
            string Data = "{\"Data\":" + Newtonsoft.Json.JsonConvert.SerializeObject(Data1) + "}"; ;
            return Ok(Data);
        }
    }
}
