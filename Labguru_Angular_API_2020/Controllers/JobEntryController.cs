﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Labguru_Angular_API_2020.DbContexts;
using Labguru_Angular_DTO;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Options;
using Labguru_Angular_API_2020.Models;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Drawing;

namespace Labguru_Angular_API_2020.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobEntryController : ControllerBase
    {
        private readonly SqlDbContext _context;



        public JobEntryController(SqlDbContext context)
        {
            _context = context;

        }

        [Route("[action]")]
        [HttpPost]
        public IActionResult GetJobEntry_Customer(CommonList_Input objCommonInput)
        {
            //URL /api/JobEntry/GetJobEntry_Customer
            try
            {


                string input_json =
                          "{Data:" + Newtonsoft.Json.JsonConvert.SerializeObject(objCommonInput) + "}";
                JToken json_Convert = JObject.Parse(input_json);
                var jsonData = new SqlParameter("@jsonData", Convert.ToString(json_Convert));

                SqlDbContext _context_RxTpe = new SqlDbContext();
                var SituationID_Customer = new SqlParameter("@SituationID", 13);
                var Data_Customer = _context_RxTpe.Common.FromSqlRaw<CommonOutPut>("exec USP_SELECT_COMMON_LIST_2020 @jsonData, @SituationID", jsonData, SituationID_Customer).ToList();

                string Data =
                        "{Customer:" + Newtonsoft.Json.JsonConvert.SerializeObject(Data_Customer) + "}";
                JToken json = JObject.Parse(Data);

                Output_Data output = new Output_Data();
                output.message = "success";
                output.status = 200;
                output.data = json;

                return Ok(output);

            }
            catch (Exception ex)
            {

                Output_Data output = new Output_Data();
                output.message = ex.Message;
                output.status = 409;
                output.data = "";

                return Ok(output);
            }
        }

        [Route("[action]")]
        [HttpPost]
        public IActionResult GetJobEntry_Customer_Info(CommonList_Input objCommonInput)
        {
            //URL /api/JobEntry/GetJobEntry_Customer_Info 
            List<CommonOutPut> Data_Customer_Doctors = new List<CommonOutPut>();
            List<CommonOutPut> Data_Customer_Products = new List<CommonOutPut>();
            List<CommonOutPut> Data_Customer_Shade = new List<CommonOutPut>();
            List<CommonOutPut> Data_Customer_Stage = new List<CommonOutPut>();
            List<CommonOutPut> Data_Customer_Attentions = new List<CommonOutPut>();
            List<CommonOutPut> Data_Customer_Teeth_Top = new List<CommonOutPut>();
            List<CommonOutPut> Data_Customer_Teeth_Bottom = new List<CommonOutPut>();




            try
            {


                string input_json =
                         "{Data:" + Newtonsoft.Json.JsonConvert.SerializeObject(objCommonInput) + "}";
                JToken json_Convert = JObject.Parse(input_json);
                var jsonData = new SqlParameter("@jsonData", Convert.ToString(json_Convert));

                SqlDbContext _context_Validation = new SqlDbContext();
                var SituationID_Customer = new SqlParameter("@SituationID", 14);
                var Data_Customer_Validation = _context_Validation.Common.FromSqlRaw<CommonOutPut>("exec USP_SELECT_COMMON_LIST_2020 @jsonData, @SituationID", jsonData, SituationID_Customer).ToList();

                List<Common_Validation_Message> objValidationMessage = new List<Common_Validation_Message>();


                if (Data_Customer_Validation.Count == 0)
                {
                    SqlDbContext _context_Doctors = new SqlDbContext();
                    var SituationID_Customer_Doctors = new SqlParameter("@SituationID", 15);//List of Doctors
                    Data_Customer_Doctors = _context_Doctors.Common.FromSqlRaw<CommonOutPut>("exec USP_SELECT_COMMON_LIST_2020 @jsonData, @SituationID", jsonData, SituationID_Customer_Doctors).ToList();

                    SqlDbContext _context_Products = new SqlDbContext();
                    var SituationID_Customer_Products = new SqlParameter("@SituationID", 16);// List of List of Products
                    Data_Customer_Products = _context_Products.Common.FromSqlRaw<CommonOutPut>("exec USP_SELECT_COMMON_LIST_2020 @jsonData, @SituationID", jsonData, SituationID_Customer_Products).ToList();

                    SqlDbContext _context_Shade = new SqlDbContext();
                    var SituationID_Customer_Shade = new SqlParameter("@SituationID", 17);// List of Shade
                    Data_Customer_Shade = _context_Shade.Common.FromSqlRaw<CommonOutPut>("exec USP_SELECT_COMMON_LIST_2020 @jsonData, @SituationID ", jsonData, SituationID_Customer_Shade).ToList();

                    SqlDbContext _context_Stage = new SqlDbContext();
                    var SituationID_Customer_Stage = new SqlParameter("@SituationID", 18);// List of Stage
                    Data_Customer_Stage = _context_Stage.Common.FromSqlRaw<CommonOutPut>("exec USP_SELECT_COMMON_LIST_2020 @jsonData, @SituationID ", jsonData, SituationID_Customer_Stage).ToList();

                    SqlDbContext _context_Attentions = new SqlDbContext();
                    var SituationID_Customer_Attentions = new SqlParameter("@SituationID", 19);// List of Attentions
                    Data_Customer_Attentions = _context_Attentions.Common.FromSqlRaw<CommonOutPut>("exec USP_SELECT_COMMON_LIST_2020 @jsonData, @SituationID ", jsonData, SituationID_Customer_Attentions).ToList();

                    SqlDbContext _context_Teeth_Top = new SqlDbContext();
                    var SituationID_Customer_Teeth_Top = new SqlParameter("@SituationID", 20);// List of Attentions
                    Data_Customer_Teeth_Top = _context_Teeth_Top.Common.FromSqlRaw<CommonOutPut>("exec USP_SELECT_COMMON_LIST_2020 @jsonData, @SituationID ", jsonData, SituationID_Customer_Teeth_Top).ToList();


                    SqlDbContext _context_Teeth_Bottom = new SqlDbContext();
                    var SituationID_Customer_Teeth_Bottom = new SqlParameter("@SituationID", 21);// List of Attentions
                    Data_Customer_Teeth_Bottom = _context_Teeth_Bottom.Common.FromSqlRaw<CommonOutPut>("exec USP_SELECT_COMMON_LIST_2020 @jsonData, @SituationID ", jsonData, SituationID_Customer_Teeth_Bottom).ToList();



                }
                else
                {
                    objValidationMessage.Add(new Common_Validation_Message { Message = Data_Customer_Validation[0].name });
                }


                string Data =
                       "{Validation:" + Newtonsoft.Json.JsonConvert.SerializeObject(objValidationMessage) + "," +
                       "Doctors:" + Newtonsoft.Json.JsonConvert.SerializeObject(Data_Customer_Doctors) + "," +
                       "Products:" + Newtonsoft.Json.JsonConvert.SerializeObject(Data_Customer_Products) + "," +
                       "Shade:" + Newtonsoft.Json.JsonConvert.SerializeObject(Data_Customer_Shade) + "," +
                       "Stage:" + Newtonsoft.Json.JsonConvert.SerializeObject(Data_Customer_Stage) + "," +
                       "Attentions:" + Newtonsoft.Json.JsonConvert.SerializeObject(Data_Customer_Attentions) + "," +
                       "Teeth_Top_CV:" + Newtonsoft.Json.JsonConvert.SerializeObject(Data_Customer_Teeth_Top) + "," +
                       "Teeth_Bottom_ACV:" + Newtonsoft.Json.JsonConvert.SerializeObject(Data_Customer_Teeth_Bottom) + "}";

                JToken json = JObject.Parse(Data);

                Output_Data output = new Output_Data();
                output.message = "success";
                output.status = 200;
                output.data = json;

                return Ok(output);

            }
            catch (Exception ex)
            {

                Output_Data output = new Output_Data();
                output.message = ex.Message;
                output.status = 409;
                output.data = "";

                return Ok(output);
            }
        }

        [Route("[action]")]
        [HttpPost]
        public IActionResult GetJobEntry_Implant_Info(JobEntry_Implant_Info_Input objImplant_Info)
        {
            //URL /api/JobEntry/GetJobEntry_Customer_Info 
            List<CommonOutPut> Data_Customer_Doctors = new List<CommonOutPut>();
            List<JobEntry_Implant_Info_OutPut> Implant_Company = new List<JobEntry_Implant_Info_OutPut>();
            List<JobEntry_Implant_Company_Detail> Implant_Child = new List<JobEntry_Implant_Company_Detail>();
            List<JobEntry_Implant_Company> Final = new List<JobEntry_Implant_Company>();

            try
            {



                SqlDbContext _context_Implant = new SqlDbContext();
                var ImplantCompanyID = new SqlParameter("@ImplantCompanyID", "0");
                var SituationID = new SqlParameter("@SituationID", 4);
                var JobEntryID = new SqlParameter("@JobEntryID", objImplant_Info.JobEntryID);
                var ReferenceID = new SqlParameter("@JobEntryReferenceID", objImplant_Info.JobEntryReferenceID);

                var Data_JobEntry_Implant = _context_Implant.JobEntry_Implant_Info.FromSqlRaw<JobEntry_Implant_Info_OutPut>("exec Usp_select_PP_TJobEntry_ImplantEntry @ImplantCompanyID, @SituationID,@JobEntryID, @JobEntryReferenceID", ImplantCompanyID, SituationID, JobEntryID, ReferenceID).ToList();

                // var Distinct_Company = Data_JobEntry_Implant.Where(i => i.Code != "").Distinct().ToList();
                var Distinct_Company = Data_JobEntry_Implant.Select(x => x.Code).Distinct().ToList();
                for (int i = 0; i < Distinct_Company.Count; i++)
                {
                    Implant_Company = Data_JobEntry_Implant.Where(z => z.Code == Distinct_Company[i].ToString()).ToList();
                    Implant_Child = new List<JobEntry_Implant_Company_Detail>();
                    for (int j = 0; j < Implant_Company.Count; j++)
                    {

                        Implant_Child.Add(new JobEntry_Implant_Company_Detail
                        {
                            Select = Implant_Company[j].Select,
                            ImplantCompanyID = Implant_Company[j].ImplantCompanyID,
                            ImplantCompanyDetailID = Implant_Company[j].ImplantCompanyDetailID,
                            Size = Implant_Company[j].Size,
                            Platform = Implant_Company[j].Platform,
                            Quantity = Implant_Company[j].Quantity
                        });
                    }

                    // JobEntry_Implant_Company Implant_Company1 = new JobEntry_Implant_Company();
                    //Implant_Company1.Select = Distinct_Company[i].Select;
                    //Implant_Company1.ImplantCompanyID = Distinct_Company[i].ImplantCompanyID;
                    //Implant_Company1.Code = Distinct_Company[i].Code;
                    //Implant_Company1.ImplantCompany = Distinct_Company[i].ImplantCompany;

                    Final.Add(new JobEntry_Implant_Company
                    {
                        Select = Implant_Company[0].Select,
                        ImplantCompanyID = Implant_Company[0].ImplantCompanyID,
                        Code = Implant_Company[0].Code,
                        ImplantCompany = Implant_Company[0].ImplantCompany,
                        Child = Implant_Child
                    });
                }
                string Data =
                   "{Implant:" + Newtonsoft.Json.JsonConvert.SerializeObject(Final) + "}";

                JToken json = JObject.Parse(Data);

                Output_Data output = new Output_Data();
                output.message = "success";
                output.status = 200;
                output.data = json;

                return Ok(output);

            }
            catch (Exception ex)
            {

                Output_Data output = new Output_Data();
                output.message = ex.Message;
                output.status = 409;
                output.data = "";

                return Ok(output);
            }
        }


        [Route("[action]")]
        [HttpPost]
        public IActionResult GetJobEntry_Rework_Info(JobEntry_Rework_Info_Input objRework_Info_Input)
        {
            List<Common_Validation_Output> objCommon_Validation_Output = new List<Common_Validation_Output>(); 
            List<JobEntry_Rework_Header> Data_JobEntry_Rework_Header = new List<JobEntry_Rework_Header>(); 
            List<Common_Validation_Message> objValidationMessage = new List<Common_Validation_Message>();
            string Data = ""; JToken json;
            Output_Data output = new Output_Data();
            try
            {



                SqlDbContext _context_Common_Validation = new SqlDbContext();
                var OrganizationUnitID = new SqlParameter("@OrganizationUnitID", objRework_Info_Input.OrganizationUnitID);
                var TransactionNumber = new SqlParameter("@TransactionNumber", objRework_Info_Input.TransactionNumber);
                var SituationID = new SqlParameter("@SituationID", "0");

                objCommon_Validation_Output = _context_Common_Validation.Get_Common_Validation.FromSqlRaw<Common_Validation_Output>("exec usp_Common_Validation_2020 @OrganizationUnitID, @TransactionNumber,@SituationID", OrganizationUnitID, TransactionNumber, SituationID).ToList();
                _context_Common_Validation.Dispose();


                if (objCommon_Validation_Output[0].Message == "")
                {
                    
                    SqlDbContext _context_JobEntry_Rework_Header = new SqlDbContext();
                    JobEntry_Rework_Info_Input objJobEntry_Rework_Info_Input_New = new JobEntry_Rework_Info_Input();
                    objJobEntry_Rework_Info_Input_New.TransactionNumber = objRework_Info_Input.TransactionNumber;
                    objJobEntry_Rework_Info_Input_New.ModuleID = objCommon_Validation_Output[0].JobEntryID;
                    objJobEntry_Rework_Info_Input_New.ModuleReferenceID = objCommon_Validation_Output[0].JobEntryReferenceID;
                    objJobEntry_Rework_Info_Input_New.LoginUserID = objRework_Info_Input.LoginUserID;

                    string input_json_H =
                       "{Data:" + Newtonsoft.Json.JsonConvert.SerializeObject(objJobEntry_Rework_Info_Input_New) + "}";
                    JToken json_Convert_Redo = JObject.Parse(input_json_H);

                    var jsonData_H = new SqlParameter("@jsonData", Convert.ToString(json_Convert_Redo));
                    var SituationID_H = new SqlParameter("@SituationID", "0");

                    Data_JobEntry_Rework_Header = _context_JobEntry_Rework_Header.JobEntry_Rework_Header.FromSqlRaw<JobEntry_Rework_Header>("exec usp_select_PP_TJobEntry_2020 @jsonData,@SituationID", jsonData_H, SituationID_H).ToList();

                    json = JObject.Parse(Data_JobEntry_Rework_Header[0].Result);
                    output.message = "success";
                    output.status = 200;
                    output.data = json;

                }
                else

                {
                    objValidationMessage.Add(new Common_Validation_Message { Message = objCommon_Validation_Output[0].Message });
                    Data =
                    "{Validation:" + Newtonsoft.Json.JsonConvert.SerializeObject(objValidationMessage)+"}";
                    json = JObject.Parse(Data);

                    output.message = objCommon_Validation_Output[0].Message;
                    output.status = 409;
                    output.data = "";
                }
                

               
              

                return Ok(output);

            }
            catch (Exception ex)
            {
               
                output.message = ex.Message;
                output.status = 409;
                output.data = "";

                return Ok(output);
            }
        }


        [Route("[action]")]
        [HttpPost]
        public IActionResult GetJobEntry_Product_Price(JobEntry_Product_Price_Input objJobEntry_Product_Price_Input)
        {
            List<JobEntry_Product_Price_Output> objJobEntry_Product_Price_Output = new List<JobEntry_Product_Price_Output>();
            try
            {
                SqlDbContext _context_Common_Validation = new SqlDbContext();
                var LoginUserID = new SqlParameter("@LoginUserID", objJobEntry_Product_Price_Input.LoginUserID);
                var CustomeBranchID = new SqlParameter("@CustomeBranchID", objJobEntry_Product_Price_Input.CustomeBranchID);
                var ProductID = new SqlParameter("@ProductID", objJobEntry_Product_Price_Input.ProductID);
                var Transactiondate = new SqlParameter("@Transactiondate", objJobEntry_Product_Price_Input.Transactiondate);

                objJobEntry_Product_Price_Output = _context_Common_Validation.Get_JobEntry_Product_Price_Output.FromSqlRaw<JobEntry_Product_Price_Output>("exec usp_select_PP_MProduct_Price_2020 @LoginUserID, @CustomeBranchID,@ProductID,@Transactiondate", LoginUserID, CustomeBranchID, ProductID, Transactiondate).ToList();

                string Data =
                      "{Data:" + Newtonsoft.Json.JsonConvert.SerializeObject(objJobEntry_Product_Price_Output) + "}";

                JToken json = JObject.Parse(Data);

                Output_Data output = new Output_Data();
                output.message = "success";
                output.status = 200;
                output.data = json;

                return Ok(output);

            }
            catch (Exception ex)
            {

                Output_Data output = new Output_Data();
                output.message = ex.Message;
                output.status = 409;
                output.data = "";

                return Ok(output);
            }
        }

        [HttpPost]
        [RequestFormLimits(MultipartBodyLengthLimit = 10000000000)]
        [DisableRequestSizeLimit]
        [Consumes("multipart/form-data")]
        [Route("[action]")]
        public IActionResult JobEntry_Insert_Update(IFormCollection fc)
        {

            try
            {

                JobEntry_Insert_Update objJobEntry_Insert_Update = new JobEntry_Insert_Update();

                objJobEntry_Insert_Update.objJobEntry_Basic_Information = (JobEntry_Basic_Information)Newtonsoft.Json.JsonConvert.DeserializeObject(fc["objJobEntry_Basic_Information"], typeof(JobEntry_Basic_Information));
                objJobEntry_Insert_Update.objJobEntry_Customer_Information = (JobEntry_Customer_Information)Newtonsoft.Json.JsonConvert.DeserializeObject(fc["objJobEntry_Customer_Information"], typeof(JobEntry_Customer_Information));
                objJobEntry_Insert_Update.objJobEntry_Product_Information = (List<JobEntry_Product_Information>)Newtonsoft.Json.JsonConvert.DeserializeObject(fc["objJobEntry_Product_Information"], typeof(List<JobEntry_Product_Information>));
                objJobEntry_Insert_Update.objJobEntry_Preference = (JobEntry_Preference)Newtonsoft.Json.JsonConvert.DeserializeObject(fc["objJobEntry_Preference"], typeof(JobEntry_Preference));

                SqlDbContext _context_JobEntry_Insert_Update = new SqlDbContext();
                var Data = Newtonsoft.Json.JsonConvert.SerializeObject(objJobEntry_Insert_Update);

                string input_json = Data;
                JToken json_Convert = JObject.Parse(input_json);

                var jsonData = new SqlParameter("@ijsondata", Convert.ToString(json_Convert));

                var Result = _context_JobEntry_Insert_Update.JobEntry_Insert_Update_Output.FromSqlRaw<JobEntry_Insert_Update_Output>("exec USP_SAVE_PP_TJOBENTRY_2020 @ijsondata ", jsonData).ToList();


                //JToken json = JObject.Parse(Result[0].message);

                Output_Data output = new Output_Data();
                output.message = Result[0].message;
                output.status = Result[0].status;
                output.data = Result[0].message;

                return Ok(output);

            }
            catch (Exception ex)
            {

                Output_Data output = new Output_Data();
                output.message = ex.Message;
                output.status = 409;
                output.data = "";
                return Ok(output);
            }
        }

        [Route("[action]")]
        [HttpPost]
        public IActionResult GetJobEntry_Product(JobEntry_Search objJobEntry_Search)
        {

            try
            {

                SqlDbContext _context_Product = new SqlDbContext();
                var JobEntryID = new SqlParameter("@JobentryID", objJobEntry_Search.JobEntryID);
                var ReferenceID = new SqlParameter("@ReferenceID", objJobEntry_Search.ReferenceID);
                var SituationID = new SqlParameter("@SituationID", "1");
                var Product = _context_Product.Common.FromSqlRaw<CommonOutPut>("exec usp_list_pp_tjobentry_product @JobentryID, @ReferenceID,@SituationID", JobEntryID, ReferenceID, SituationID).ToList();


                string Data =
                      "{Product:" + Newtonsoft.Json.JsonConvert.SerializeObject(Product) + "}";

                JToken json = JObject.Parse(Data);

                Output_Data output = new Output_Data();
                output.message = "success";
                output.status = 200;
                output.data = json;

                return Ok(output);

            }
            catch (Exception ex)
            {

                Output_Data output = new Output_Data();
                output.message = ex.Message;
                output.status = 409;
                output.data = "";

                return Ok(output);
            }
        }

        [Route("[action]")]
        [HttpPost]
        public IActionResult GetJobEntry_AppliedCoupons(JobEntry_Search objJobEntry_Search)
        {
            List<JobEntry_AppliedCoupons> objJobEntry_AppliedCoupons = new List<JobEntry_AppliedCoupons>();
            try
            {

                SqlDbContext _context_AppliedCoupons = new SqlDbContext();
                var SoftwareSubComponentID = new SqlParameter("@SoftwareSubComponentID", "3195");
                var OrganizationLanguageID = new SqlParameter("@OrganizationLanguageID", "1");
                var LoginLanguageid = new SqlParameter("@LoginLanguageid", "1");
                var CustomerID = new SqlParameter("@CustomerID", objJobEntry_Search.CustomerID);
                var JobEntryID = new SqlParameter("@JobEntryID", objJobEntry_Search.JobEntryID);
                var JobEntryReferenceID = new SqlParameter("@JobEntryReferenceID", objJobEntry_Search.ReferenceID);
                var ProductID = new SqlParameter("@ProductID", objJobEntry_Search.ProductID);
                var JobDesignID = new SqlParameter("@JobDesignID", "0");
                var JobEntryNo = new SqlParameter("@JobEntryNo", "");
                objJobEntry_AppliedCoupons = _context_AppliedCoupons.JobEntry_AppliedCoupons.FromSqlRaw<JobEntry_AppliedCoupons>("exec usp_select_PP_TJobEntry_AppliedCoupons @SoftwareSubComponentID, @OrganizationLanguageID,@LoginLanguageid,@CustomerID,@JobEntryID,@JobEntryReferenceID,@ProductID,@JobDesignID,@JobEntryNo", SoftwareSubComponentID, OrganizationLanguageID, LoginLanguageid, CustomerID, JobEntryID, JobEntryReferenceID, ProductID, JobDesignID, JobEntryNo).ToList();


                string Data =
                      "{Coupon:" + Newtonsoft.Json.JsonConvert.SerializeObject(objJobEntry_AppliedCoupons) + "}";

                JToken json = JObject.Parse(Data);

                Output_Data output = new Output_Data();
                output.message = "success";
                output.status = 200;
                output.data = json;

                return Ok(output);

            }
            catch (Exception ex)
            {

                Output_Data output = new Output_Data();
                output.message = ex.Message;
                output.status = 409;
                output.data = "";

                return Ok(output);
            }
        }

        [Route("[action]")]
        [HttpPost]
        public IActionResult GetJobEntry_SupplierWarranty(JobEntry_Search objJobEntry_Search)
        {
            List<JobEntry_SupplierWarranty> objSupplierWarranty = new List<JobEntry_SupplierWarranty>();
            try
            {

                SqlDbContext _context_SupplierWarranty = new SqlDbContext();
                var JobEntryID = new SqlParameter("@JobEntryID", objJobEntry_Search.JobEntryID);
                var JobEntryReferenceID = new SqlParameter("@JobEntryReferenceID", objJobEntry_Search.ReferenceID);
                objSupplierWarranty = _context_SupplierWarranty.JobEntry_SupplierWarranty.FromSqlRaw<JobEntry_SupplierWarranty>("exec usp_select_PP_TJobEntry_SupplierWarrantyDetails @JobEntryID,@JobEntryReferenceID", JobEntryID, JobEntryReferenceID).ToList();


                string Data =
                      "{SupplierWarranty:" + Newtonsoft.Json.JsonConvert.SerializeObject(objSupplierWarranty) + "}";

                JToken json = JObject.Parse(Data);

                Output_Data output = new Output_Data();
                output.message = "success";
                output.status = 200;
                output.data = json;

                return Ok(output);

            }
            catch (Exception ex)
            {

                Output_Data output = new Output_Data();
                output.message = ex.Message;
                output.status = 409;
                output.data = "";

                return Ok(output);
            }
        }


        [Route("[action]")]
        [HttpPost]
        public IActionResult JobEntry_Delete(JobEntry_Search objJobEntry_Search)
        {
            try
            {

                SqlDbContext _context_Delete = new SqlDbContext();
                var JobEntryID = new SqlParameter("@JobEntryID", objJobEntry_Search.JobEntryID);
                var ReferenceID = new SqlParameter("@ReferenceID", objJobEntry_Search.ReferenceID);
                _context_Delete.Database.ExecuteSqlRaw("usp_delete_pp_tjobentry @JobEntryID, @ReferenceID", JobEntryID, ReferenceID);

                string Data =
                      "{Delete:" + Newtonsoft.Json.JsonConvert.SerializeObject("Job Entry Deleted Successfully.") + "}";

                JToken json = JObject.Parse(Data);

                Output_Data output = new Output_Data();
                output.message = "Job Entry Deleted Successfully.";
                output.status = 200;
                output.data = json;

                return Ok(output);

            }
            catch (Exception ex)
            {

                Output_Data output = new Output_Data();
                output.message = ex.Message;
                output.status = 409;
                output.data = "";

                return Ok(output);
            }
        }
        [Route("[action]")]
        [HttpPost]
        public IActionResult JobEntry_Print(JobEntry_Search objJobEntry_Search)
        {
            List<JobEntry_Print> objJobEntry_Print = new List<JobEntry_Print>();
            try
            {

                SqlDbContext _context_Print = new SqlDbContext();
                var JobEntryID = new SqlParameter("@JobEntryID", objJobEntry_Search.JobEntryID);
                var ReferenceID = new SqlParameter("@ReferenceID", objJobEntry_Search.ReferenceID);
                var StageID = new SqlParameter("@StageID", "0");
                objJobEntry_Print = _context_Print.JobEntry_Print.FromSqlRaw<JobEntry_Print>("exec usp_select_PP_TJobEntry_JobCard_2020 @JobEntryID,@ReferenceID,@StageID", JobEntryID, ReferenceID, StageID).ToList();

                if (objJobEntry_Print.Count > 0)
                {
                    //objJobEntry_Print[0].Contents = objJobEntry_Print[0].Contents + objJobEntry_Print[0].SecondPage;


                    ////BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();
                    ////barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;

                    ////barcode.BarHeight = 70; //50 pixel
                    ////barcode.ImageFormat = System.Drawing.Imaging.ImageFormat.Png;
                    ////barcode.TextFont = new System.Drawing.Font("Arial", 13, FontStyle.Bold);

                    ////barcode.Data = objJobEntry_Print[0].JobEntryNo;
                    ////byte[] imageData = barcode.drawBarcodeAsBytes();
                    ////objJobEntry_Print[0].Barcode = imageData;

                    //System.Drawing.Imaging.ImageFormat format = System.Drawing.Imaging.ImageFormat.Png;
                    //byte[] imageBytes = (byte[])objJobEntry_Print[0].Barcode;
                    //using (System.IO.MemoryStream ms = new System.IO.MemoryStream(imageBytes))
                    //{
                    //    format = System.Drawing.Imaging.ImageFormat.Png;
                    //    System.Drawing.Image img = System.Drawing.Image.FromStream(ms);
                    //    if (!System.IO.Directory.Exists(objJobEntry_Print[0].FolderPath))
                    //    {
                    //        System.IO.Directory.CreateDirectory(objJobEntry_Print[0].FolderPath);
                    //    }
                    //    img.Save(objJobEntry_Print[0].CompletePath, format);
                    //}
                }
                objJobEntry_Print[0].Contents = objJobEntry_Print[0].Contents.Replace(objJobEntry_Print[0].TempURL, objJobEntry_Print[0].CompleteVirtualPath);


                string Data =
                       "{Print:" + Newtonsoft.Json.JsonConvert.SerializeObject(objJobEntry_Print) + "}";

                JToken json = JObject.Parse(Data);

                Output_Data output = new Output_Data();
                output.message = "success";
                output.status = 200;
                output.data = json;

                return Ok(output);

            }
            catch (Exception ex)
            {

                Output_Data output = new Output_Data();
                output.message = ex.Message;
                output.status = 409;
                output.data = "";

                return Ok(output);
            }
        }

        [Route("[action]")]
        [HttpPost]
        public IActionResult JobEntry_Search(JobEntry_Search objJobEntry_Search)
        {

            try
            {

                SqlDbContext _context_JobEntry_Search = new SqlDbContext();
                JobEntry_Rework_Info_Input objJobEntry_Rework_Info_Input_New = new JobEntry_Rework_Info_Input();
                objJobEntry_Rework_Info_Input_New.TransactionNumber = objJobEntry_Search.TransactionNumber;
                objJobEntry_Rework_Info_Input_New.LoginUserID = objJobEntry_Search.LoginUserID;

                string input_json =
                   "{Data:" + Newtonsoft.Json.JsonConvert.SerializeObject(objJobEntry_Rework_Info_Input_New) + "}";
                JToken json_Convert = JObject.Parse(input_json);

                var jsonData = new SqlParameter("@jsonData", Convert.ToString(json_Convert));
                var SituationID = new SqlParameter("@SituationID", "1");

                var Result = _context_JobEntry_Search.JobEntry_Search.FromSqlRaw<JobEntry_Search_Output>("exec usp_select_PP_TJobEntry_2020 @jsonData,@SituationID", jsonData, SituationID).ToList();


                string Data =
                      "{Search:" + Newtonsoft.Json.JsonConvert.SerializeObject(Result) + "}";

                Output_Data output = new Output_Data();
                if (Result[0] == null)
                {
                    output.message = "No Record Found";
                    output.status = 409;
                    output.data = "";
                }
                else
                {
                    JToken json = JObject.Parse(Result[0].Result);
                    output.message = "Job Entry Found";
                    output.status = 200;
                    output.data = json;
                }




                return Ok(output);

            }
            catch (Exception ex)
            {

                Output_Data output = new Output_Data();
                output.message = ex.Message;
                output.status = 409;
                output.data = "";

                return Ok(output);
            }
        }
    }
}
