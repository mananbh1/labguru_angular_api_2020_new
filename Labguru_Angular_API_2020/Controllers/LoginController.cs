﻿using Labguru_Angular_API_2020.DbContexts;
using Labguru_Angular_DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
//using Microsoft.WindowsAzure.Storage;
//using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace Labguru_Angular_API_2020.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        public SqlDbContext _sqlcontext = new SqlDbContext();
        private Microsoft.Extensions.Configuration.IConfiguration _config;

        public LoginController(IConfiguration config)
        {
            _config = config;
        }





        // GET: api/Login/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Login
        [Route("[action]")]
        [HttpPost]
        public IActionResult PostLogin(Login_Input UserDTO)
        {
            //URL api/login/PostLogin

            List<Login_Output> objLogin_Output = new List<Login_Output>();
            List<Menu> objMenu = new List<Menu>();
            try
            {


                var UserCode = new SqlParameter("@UserCode", UserDTO.Code);
                var Password = new SqlParameter("@Password", UserDTO.Password);
                var IPAddress = new SqlParameter("@IPAddress", UserDTO.IP);

                var SituationID = new SqlParameter("@SituationID", "0");
                objLogin_Output = _sqlcontext.Login_Output.FromSqlRaw("exec usp_Validate_Login_2020 @UserCode,@Password,@IPAddress, @SituationID", UserCode, Password, IPAddress, SituationID).ToList();

                if (objLogin_Output[0].ValidationMsg == "")
                {
                    var Menu_SituationID = new SqlParameter("@SituationID", "1");
                    objMenu = _sqlcontext.Menu.FromSqlRaw("exec usp_Validate_Login_2020 @UserCode,@Password,@IPAddress, @SituationID", UserCode, Password, IPAddress, Menu_SituationID).ToList();

                }

                //var distncparent = Menu.Where(i => i.ParentMenuID == 0).Distinct().ToList();

                //List<UserMenu> FinaluserMenu = new List<UserMenu>();
                //UserMenu userMenu = new UserMenu();

                //for (int i = 0; i < distncparent.Count; i++)
                //{

                //    List<Menu> menu = new List<Menu>();
                //    List<Menu> chilMenu = new List<Menu>();
                //    chilMenu = Menu.Where(z => z.ParentMenuID == distncparent[i].MenuID).ToList();
                //    for (int j = 0; j < chilMenu.Count; j++)
                //    {
                //        menu.Add(new Menu
                //        {
                //            MenuID = chilMenu[j].MenuID,
                //            MenuName = chilMenu[j].MenuName,
                //            Collapse = chilMenu[j].Collapse,
                //            Icon = chilMenu[j].Icon,
                //            ParentMenuID = distncparent[i].ParentMenuID,
                //            Route = chilMenu[j].Route
                //        });
                //    }

                //    userMenu.Collapse = distncparent[i].Collapse;
                //    userMenu.MenuName = distncparent[i].MenuName;
                //    userMenu.Icon = distncparent[i].Icon;
                //    userMenu.Collapse = distncparent[i].Collapse;
                //    userMenu.Route = distncparent[i].Route;
                //    userMenu.ChildMenu = menu;

                //    FinaluserMenu.Add(new UserMenu
                //    {
                //        Collapse = distncparent[i].Collapse,
                //        MenuName = distncparent[i].MenuName,
                //        Icon = distncparent[i].Icon,
                //        Route = distncparent[i].Route,
                //        ChildMenu = menu
                //    });

                //}
                //List<Login_Output> objlogin = new List<Login_Output>();
                //objlogin.Add(new Login_Output { Code = UserDTO.Code, LoginUserID = 1, LoginUser = "Administrator", Message = "" });


                //List<UserMenu> FinaluserMenu = new List<UserMenu>();
                //// UserMenu userMenu = new UserMenu();





                var Token = GenerateJSONWebToken(UserDTO);
                string Data =
                        "{\"Login_Data\":" + Newtonsoft.Json.JsonConvert.SerializeObject(objLogin_Output) + ", " +
                        "\"Menu_Data\":" + Newtonsoft.Json.JsonConvert.SerializeObject(objMenu) + ", " +
                        "\"Token\":" + Newtonsoft.Json.JsonConvert.SerializeObject(Token) + "}";

                JToken json = JObject.Parse(Data);

                Output_Data output = new Output_Data();
                output.message = "success";
                output.status = 200;
                output.data = json;

                return Ok(output);
            }
            catch (Exception ex)
            {

                Output_Data output = new Output_Data();
                output.message = ex.Message;
                output.status = 409;
                output.data = "";

                return Ok(output);
            }

        }
        private string GenerateJSONWebToken(Login_Input loginInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[] {
             new Claim(JwtRegisteredClaimNames.Sub, loginInfo.Code),
             new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
             };

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
            _config["Jwt:Issuer"],
            claims,
            expires: DateTime.Now.AddMinutes(120),
            signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        // PUT: api/Login/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }


        [Route("[action]")]
        [HttpPost]
        public IActionResult Profile_Forgot_Password(UserProfile_Input objUserProfile_Input)
        {

            try
            {
                string input_json =
                       "{Data:" + Newtonsoft.Json.JsonConvert.SerializeObject(objUserProfile_Input) + "}";
                JToken json_Convert = JObject.Parse(input_json);


                SqlDbContext _context_Profile = new SqlDbContext();
                var jsonData = new SqlParameter("@jsonData", Convert.ToString(json_Convert));
                var SituationID = new SqlParameter("@SituationID", 1);

                var UserProfile_Output = _context_Profile.UserProfile_Output.FromSqlRaw("exec usp_Admin_MUser_Profile_Setting_2020 @jsonData,@SituationID", jsonData, SituationID).ToList();


                Output_Data output = new Output_Data();
                output.message = UserProfile_Output[0].message;
                output.status = UserProfile_Output[0].status;
                output.data = "";

                return Ok(output);

            }
            catch (Exception ex)
            {

                Output_Data output = new Output_Data();
                output.message = ex.Message;
                output.status = 409;
                output.data = "";

                return Ok(output);
            }
        }

        [Route("[action]")]
        [HttpPost]
        public IActionResult Profile_Change_Password(UserProfile_Input objUserProfile_Input)
        {

            try
            {
                string input_json =
                      "{Data:" + Newtonsoft.Json.JsonConvert.SerializeObject(objUserProfile_Input) + "}";
                JToken json_Convert = JObject.Parse(input_json);

                SqlDbContext _context_Profile = new SqlDbContext();
                var jsonData = new SqlParameter("@jsonData", Convert.ToString(json_Convert));
                var SituationID = new SqlParameter("@SituationID", 2);

                var UserProfile_Output = _context_Profile.UserProfile_Output.FromSqlRaw("exec usp_Admin_MUser_Profile_Setting_2020 @jsonData, @SituationID", jsonData, SituationID).ToList();


                Output_Data output = new Output_Data();
                output.message = UserProfile_Output[0].message;
                output.status = UserProfile_Output[0].status;
                output.data = "";

                return Ok(output);

            }
            catch (Exception ex)
            {

                Output_Data output = new Output_Data();
                output.message = ex.Message;
                output.status = 409;
                output.data = "";

                return Ok(output);
            }
        }

        [HttpPost]
        [RequestFormLimits(MultipartBodyLengthLimit = 10000000000)]
        [DisableRequestSizeLimit]
        [Consumes("multipart/form-data")]
        [Route("[action]")]
        public IActionResult Profile_Update_Detail(IFormCollection fc)
        {

            try
            {
                UserProfile_Input objUserProfile_Input = new UserProfile_Input();
                string VirtualPath = "";
                objUserProfile_Input.Code = Convert.ToString(fc["Code"]);
                objUserProfile_Input.LoginUser = Convert.ToString(fc["LoginUser"]);
                objUserProfile_Input.MobileNumber = Convert.ToString(fc["MobileNumber"]);
                objUserProfile_Input.EmailID = Convert.ToString(fc["EmailID"]);

                if (fc.Files.Count > 0)
                {

                    SqlDbContext _context_Document = new SqlDbContext();
                    var FolderId = new SqlParameter("@FolderId", 98); // User_Profile_2020 = 98
                    var Document = _context_Document.Document_FolderPath.FromSqlRaw<Document_FolderPath>("exec usp_select_Document_FolderPath @FolderId", FolderId).ToList();

                    if (Document.Count > 0)
                    {
                        objUserProfile_Input.Profile = Convert.ToString(objUserProfile_Input.Code + "." + fc.Files[0].FileName.Substring(fc.Files[0].FileName.LastIndexOf(".") + 1));
                        string PhysicalPath = Document[0].PhysicalFilePath;
                        string path = PhysicalPath + objUserProfile_Input.Profile;
                        VirtualPath = Document[0].VirtualPath + Document[0].FolderPath + "/" + objUserProfile_Input.Profile;
                        if (!System.IO.Directory.Exists(PhysicalPath))
                        {
                            System.IO.Directory.CreateDirectory(PhysicalPath);
                        }

                        var stream = new FileStream(path, FileMode.Create);
                        fc.Files[0].CopyTo(stream);
                        stream.Close();

                        if (Document[0].Documents_location == 2)
                        {
                            //#region Cloud storage code...
                            //string StorageConnectionString = Convert.ToString(Document[0].StorageConnectionString);
                            //string ContainerName = Convert.ToString(Document[0].ContainerName);
                            //string AzureFolderPath = Convert.ToString(Document[0].FolderPath);
                            //CloudStorageAccount storageAccount = CloudStorageAccount.Parse(StorageConnectionString);

                            //CloudBlobClient bc = storageAccount.CreateCloudBlobClient();
                            //CloudBlobContainer container = bc.GetContainerReference(System.IO.Path.Combine(ContainerName, AzureFolderPath));

                            //// string fullpath = PhysicalPath + "/" + Convert.ToString(fc.Files[0].FileName );//Context.Server.MapPath(Context.Request.ApplicationPath + "/Repository/Rx/" + FileName + "." + FileType);
                            //string key = Path.GetFileName(path);
                            //new Models.AzureStorage().UploadBlob(container, key, path);


                            //if (System.IO.File.Exists(PhysicalPath))
                            //    System.IO.File.Delete(PhysicalPath);
                            //#endregion
                        }
                    }
                }



                SqlDbContext _context_Profile = new SqlDbContext();
                string input_json =
                      "{Data:" + Newtonsoft.Json.JsonConvert.SerializeObject(objUserProfile_Input) + "}";

                JToken json_Convert = JObject.Parse(input_json);
                var jsonData = new SqlParameter("@jsonData", Convert.ToString(json_Convert));
                var SituationID = new SqlParameter("@SituationID", 3);



                var UserProfile_Output = _context_Profile.UserProfile_Output.FromSqlRaw("exec usp_Admin_MUser_Profile_Setting_2020 @jsonData,@SituationID", jsonData, SituationID).ToList();
                objUserProfile_Input.Profile = VirtualPath;
                string Data =
                       "{\"Input_Data\":" + Newtonsoft.Json.JsonConvert.SerializeObject(objUserProfile_Input) + "}";

                JToken json = JObject.Parse(Data);

                Output_Data output = new Output_Data();
                output.message = UserProfile_Output[0].message;
                //  output.message = fc.Files[0].FileName + ";" + fc.Files[0].ContentType + ";" + fc.Files[0].Length;
                output.status = UserProfile_Output[0].status;
                output.data = json;

                return Ok(output);

            }
            catch (Exception ex)
            {

                Output_Data output = new Output_Data();
                output.message = ex.Message;
                output.status = 409;
                output.data = "";

                return Ok(output);
            }
        }

        [Route("[action]")]
        [HttpPost]
        public IActionResult Profile_Preference_Data(CommonList_Input objCommonInput)
        {
            try
            {
                string input_json =
                        "{Data:" + Newtonsoft.Json.JsonConvert.SerializeObject(objCommonInput) + "}";
                JToken json_Convert = JObject.Parse(input_json);
                var jsonData = new SqlParameter("@jsonData", Convert.ToString(json_Convert));

                SqlDbContext _context_RxTpe = new SqlDbContext();
                var SituationID_RxType = new SqlParameter("@SituationID", 1); //-01.Rx Type 
                var Data_RxType = _context_RxTpe.Common.FromSqlRaw<CommonOutPut>("exec USP_SELECT_COMMON_LIST_2020 @jsonData, @SituationID", jsonData, SituationID_RxType).ToList();


                SqlDbContext _context_OU = new SqlDbContext();
                var SituationID_OU = new SqlParameter("@SituationID", 2); //02. OU
                var Data_OU = _context_OU.Common.FromSqlRaw<CommonOutPut>("exec USP_SELECT_COMMON_LIST_2020 @jsonData,@SituationID", jsonData, SituationID_OU).ToList();


                SqlDbContext _context_Location = new SqlDbContext();
                var SituationID_Location = new SqlParameter("@SituationID", 3); //03. Location
                var Data_Location = _context_Location.Common.FromSqlRaw<CommonOutPut>("exec USP_SELECT_COMMON_LIST_2020 @jsonData,@SituationID", jsonData, SituationID_Location).ToList();



                string Data =
                        "{RxType:" + Newtonsoft.Json.JsonConvert.SerializeObject(Data_RxType) + "," +
                        "OU:" + Newtonsoft.Json.JsonConvert.SerializeObject(Data_OU) + "," +
                        "Location:" + Newtonsoft.Json.JsonConvert.SerializeObject(Data_Location) + "}";

                JToken json = JObject.Parse(Data);

                Output_Data output = new Output_Data();
                output.message = "success";
                output.status = 200;
                output.data = json;

                return Ok(output);

            }
            catch (Exception ex)
            {

                Output_Data output = new Output_Data();
                output.message = ex.Message;
                output.status = 409;
                output.data = "";

                return Ok(output);
            }
        }


    }
}
