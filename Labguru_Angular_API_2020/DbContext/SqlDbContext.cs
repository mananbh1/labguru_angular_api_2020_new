﻿
using Labguru_Angular_DTO;
using Microsoft.EntityFrameworkCore;

namespace Labguru_Angular_API_2020.DbContexts
{
    public class SqlDbContext : DbContext
    {

        public SqlDbContext(DbContextOptions<SqlDbContext> options) : base(options)
        {

            //Database.Migrate();
        }

        public SqlDbContext()
        {

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
                // optionsBuilder.UseSqlServer(@"Data Source=172.22.0.68;Initial Catalog=Labguru;Integrated Security=false;User ID=development;Password=itsverytough");//lIVE
                optionsBuilder.UseSqlServer(@"Data Source=192.168.125.132;Initial Catalog=Illusion;Integrated Security=false;User ID=development;Password=development"); //UAT        
                //    optionsBuilder.UseSqlServer(@"Data Source=DESKTOP-E20LI6F;Initial Catalog=Labguru;Integrated Security=false;User ID=sa;Password=wfh"); //UAT        
                 optionsBuilder.UseSqlServer(@"Data Source=MANAN;Initial Catalog=Labguru;Integrated Security=true;");   
        
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // modelBuilder.Entity<ProductProcessMappingSelect>().HasNoKey();            
            // modelBuilder.Entity<addProductProcessMapping>().HasKey(u => new { u.ProductID}); 

            modelBuilder.Entity<Document_FolderPath>().HasNoKey();
            modelBuilder.Entity<DDLMaster>().HasNoKey();
            modelBuilder.Entity<Menu>().HasNoKey();
            modelBuilder.Entity<Login_Output>().HasNoKey();
            modelBuilder.Entity<UserProfile_Output>().HasNoKey();
            modelBuilder.Entity<CommonValidationOutput>().HasNoKey();
            modelBuilder.Entity<JobEntry_Implant_Info_OutPut>().HasNoKey();
            modelBuilder.Entity<Common_Validation_Output>().HasNoKey();
            modelBuilder.Entity<JobEntry_Product_Price_Output>().HasNoKey();
            modelBuilder.Entity<JobEntry_Rework_Header>().HasNoKey();
            modelBuilder.Entity<JobEntry_Rework_Detail>().HasNoKey();
            modelBuilder.Entity<JobEntry_AppliedCoupons>().HasNoKey();
            modelBuilder.Entity<JobEntry_SupplierWarranty>().HasNoKey();
            modelBuilder.Entity<JobEntry_Print>().HasNoKey();
            modelBuilder.Entity<JobEntry_Search_Output>().HasNoKey();
            modelBuilder.Entity<JobEntry_Insert_Update_Output>().HasNoKey();
            modelBuilder.Entity<AlignerRX_Insert_Update_Output>().HasNoKey();
            modelBuilder.Entity<AlignerRXForm_Data>().HasNoKey();
            modelBuilder.Entity<AlignerDashboard_status>().HasNoKey();
            
        }

        #region Login Page
        public DbSet<Login_Output> Login_Output { get; set; }
        public DbSet<Menu> Menu { get; set; }

        public DbSet<UserProfile_Output> UserProfile_Output { get; set; }

        #endregion

        #region Common Utility
        public DbSet<CommonOutPut> Common { get; set; }
        public DbSet<CommonValidationOutput> CommonValidationOutput { get; set; }
        public DbSet<DDLMaster> DDLMaster { get; set; }
        public DbSet<Document_FolderPath> Document_FolderPath { get; set; }

        #endregion

        #region Job Entry Page
        public DbSet<JobEntry_Implant_Info_OutPut> JobEntry_Implant_Info { get; set; }
        public DbSet<Common_Validation_Output> Get_Common_Validation { get; set; }

        public DbSet<JobEntry_Product_Price_Output> Get_JobEntry_Product_Price_Output { get; set; }

        public DbSet<JobEntry_Rework_Header> JobEntry_Rework_Header { get; set; }

        public DbSet<JobEntry_Rework_Detail> JobEntry_Rework_Detail { get; set; }

        public DbSet<JobEntry_AppliedCoupons> JobEntry_AppliedCoupons { get; set; }

        public DbSet<JobEntry_SupplierWarranty> JobEntry_SupplierWarranty { get; set; }

        public DbSet<JobEntry_Print> JobEntry_Print { get; set; }

        public DbSet<JobEntry_Search_Output> JobEntry_Search { get; set; }
        public DbSet<JobEntry_Insert_Update_Output> JobEntry_Insert_Update_Output { get; set; }

        public DbSet<AlignerRX_Insert_Update_Output> AlignerRX_Insert_Update_Output { get; set; }


        public DbSet<AlignerRXForm_Data> AlignerRXForm_Data { get; set; }
        public DbSet<AlignerDashboard_status> AlignerDashboard_status { get; set; }


        #endregion

    }
}
