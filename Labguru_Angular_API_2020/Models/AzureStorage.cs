﻿////using Microsoft.WindowsAzure.Storage;
//using Microsoft.WindowsAzure.Storage.Blob;
//using System;
//using System.Collections.Generic;
//using System.Data;
//using System.IO;
//using System.Linq;
//using System.Net;
//using System.Text;
//using System.Threading.Tasks;
//using System.Web;

//namespace Labguru_Angular_API_2020.Models
//{
//    public class AzureStorage
//    {
//        public void UploadBlob(CloudBlobContainer container, string keys, string fileName)
//        {
//            keys = keys.ToUpper();
//            CloudBlockBlob b = container.GetBlockBlobReference(keys);
//            using (var fs = System.IO.File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.None))
//            {
//                b.UploadFromStream(fs);
//            }
//        }

//        static void writeAzureDirectoriesToConsole(IEnumerable<CloudBlobContainer> containers)
//        {
//            foreach (var container in containers)
//            {
//                string indent = "";
//                Console.WriteLine("Container: " + container.Name);

//                // Pass Ienumerable to recursive function to get "subdirectories":
//                Console.WriteLine(getContainerDirectories(container.ListBlobs(), indent));
//            }
//        }

//        static string getContainerDirectories(IEnumerable<IListBlobItem> blobList, string indent)
//        {
//            // Indent each item in the output for the current subdirectory:
//            indent = indent + "  ";
//            StringBuilder sb = new StringBuilder("");

//            // First list all the actual FILES within 
//            // the current blob list. No recursion needed:
//            foreach (var item in blobList.Where
//            ((blobItem, type) => blobItem is CloudBlockBlob))
//            {
//                var blobFile = item as CloudBlockBlob;
//                sb.AppendLine(indent + blobFile.Name);
//            }

//            // List all additional subdirectories 
//            // in the current directory, and call recursively:
//            foreach (var item in blobList.Where
//            ((blobItem, type) => blobItem is CloudBlobDirectory))
//            {
//                var directory = item as CloudBlobDirectory;
//                sb.AppendLine(indent + directory.Prefix.ToUpper());

//                // Call this method recursively to retrieve subdirectories within the current:
//                sb.AppendLine(getContainerDirectories(directory.ListBlobs(), indent));
//            }
//            return sb.ToString();
//        }

//        //public byte[] GetBytesFromFilePath(string Path)
//        //{
//        //    string AzurePath = "";
//        //    DataSet dsServerPath = new BL_Common.Form_Common_Method().SelectServerFolderPath();

//        //    if (dsServerPath != null)
//        //    {
//        //        AzurePath = dsServerPath.Tables[0].Rows[0]["AzurePath"].ToString();
//        //    }

//        //    byte[] photo = { 1, 2, 3 }; ;
//        //    if (!Path.Contains(AzurePath))
//        //    {
//        //        FileInfo myfile = new FileInfo(Path);
//        //        if (myfile.Exists)
//        //        {
//        //            photo = File.ReadAllBytes(Path);
//        //            return photo;
//        //        }
//        //    }
//        //    else if (URLExists(Path))
//        //    {
//        //        using (var webClient = new WebClient())
//        //        {
//        //            photo = webClient.DownloadData(Path);
//        //        }
//        //    }

//        //    return photo;
//        //}


//        public bool URLExists(string url)
//        {
//            HttpWebResponse response = null;
//            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
//            request.Method = "HEAD";

//            bool exists;
//            try
//            {
//                System.Net.ServicePointManager.Expect100Continue = false;
//                response = (HttpWebResponse)request.GetResponse();
//                exists = true;
//            }
//            catch (Exception ex)
//            {
//                exists = false;
//            }
//            finally
//            {
//                if (response != null)
//                {
//                    response.Close();
//                }
//            }
//            return exists;
//        }

//        //public void UploadFileOnAzure(int FolderID, string filName, string FileSavePath, string Directory)
//        //{
//        //    DataSet dsServerPath = new BL_Common.Form_Common_Method().GetServerFolderPath(FolderID);
//        //    if (dsServerPath != null)
//        //    {
//        //        int DocumentslocationID = Convert.ToInt32(dsServerPath.Tables[0].Rows[0]["Documents_location"].ToString() == "" ? "0" : dsServerPath.Tables[0].Rows[0]["Documents_location"]);
//        //        if (DocumentslocationID == (int)BL_Common.Common.Enum_Document_Location.Azure_Location)
//        //        {
//        //            string StorageConnectionString = Convert.ToString(dsServerPath.Tables[0].Rows[0]["StorageConnectionString"]);
//        //            string ContainerName = Convert.ToString(dsServerPath.Tables[0].Rows[0]["ContainerName"]);
//        //            string AzureFolderPath = Convert.ToString(dsServerPath.Tables[0].Rows[0]["FolderPath"]);

//        //            if (FileSavePath.Contains(AzureFolderPath))
//        //            {
//        //                AzureFolderPath = FileSavePath;
//        //            }
//        //            else
//        //            {
//        //                AzureFolderPath = System.IO.Path.Combine(AzureFolderPath, FileSavePath);
//        //            }

//        //            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(StorageConnectionString);
//        //            CloudBlobClient bc = storageAccount.CreateCloudBlobClient();
//        //            CloudBlobContainer container = bc.GetContainerReference(System.IO.Path.Combine(ContainerName, AzureFolderPath));
//        //            //container.CreateIfNotExists();

//        //            string key = Path.GetFileName(filName);
//        //            UploadBlob(container, key, filName);

//        //            System.IO.File.Delete(filName);
//        //        }
//        //    }
//        //}

//        public bool downloadFile(string url)
//        {
//            bool downloadresult = false;
//            try
//            {
//                string file = System.IO.Path.GetFileName(url);

//                var webClient = new WebClient();
//                byte[] imageBytes = webClient.DownloadData(url);

//                string downloadImagepath = @"C:\MyImages";
//                string finalFileName = string.Empty;
//                string directoryTomake;
//                finalFileName = System.IO.Path.Combine(downloadImagepath, file);
//                directoryTomake = downloadImagepath;

//                if (!(Directory.Exists(directoryTomake)))
//                    Directory.CreateDirectory(directoryTomake);

//                if (file != null)
//                {
//                    try
//                    {
//                        using (var fileStream = System.IO.File.Create(finalFileName))
//                        {
//                            using (BinaryWriter bw = new BinaryWriter(fileStream))
//                            {
//                                bw.Write(imageBytes); bw.Close();
//                                downloadresult = true;
//                            }
//                        }
//                    }
//                    catch (Exception)
//                    {

//                        downloadresult = false;
//                    }
//                }
//            }
//            catch (Exception)
//            {
//                downloadresult = false;
//            }
//            return downloadresult;
//        }
//        public void Rename(CloudBlobContainer container, string oldName, string newName)
//        {
//            //Warning: this Wait() is bad practice and can cause deadlock issues when used from ASP.NET applications
//            RenameAsync(container, oldName, newName).Wait();
//        }

//        public async Task RenameAsync(CloudBlobContainer container, string oldName, string newName)
//        {
//            try
//            {
//                CloudBlockBlob source = (CloudBlockBlob)await container.GetBlobReferenceFromServerAsync(oldName);
//                CloudBlockBlob target = container.GetBlockBlobReference(newName);
//                CloudBlockBlob sourceBlob = container.GetBlockBlobReference(oldName);
//                target.StartCopy(sourceBlob.Uri);


//                while (target.CopyState.Status == CopyStatus.Pending)
//                    await Task.Delay(100);

//                if (target.CopyState.Status != CopyStatus.Success)
//                    throw new Exception("Rename failed: " + target.CopyState.Status);

//                await sourceBlob.DeleteAsync();
//            }
//            catch (Exception ex)
//            {
                
//            }
//        }
//    }
//}
