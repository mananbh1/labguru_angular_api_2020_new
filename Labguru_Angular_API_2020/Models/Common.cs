﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Labguru_Angular_API_2020.Models
{
    public class Common
    {
        public static Error CommonException(DbUpdateException ex)
        {
            Error obj = new Error();
            if (ex.GetBaseException().GetType() == typeof(SqlException))
            {
                Int32 ErrorCode = ((SqlException)ex.InnerException).Number;
                switch (ErrorCode)
                {
                    case 2627:
                        obj.message = "Unique constraint error";
                        obj.error = "1";
                        break;
                    case 547:
                        obj.message = "Constraint check violation";
                        obj.error = "1";
                        break;
                    case 2601:
                        obj.message = "Duplicated key row error";
                        obj.error = "1";
                        break;
                    default:
                        break;
                }
            }


            return obj;
        }

        public static string CreateXML(Object YourClassObject)
        {
            XmlDocument xmlDoc = new XmlDocument();   //Represents an XML document, 
                                                      // Initializes a new instance of the XmlDocument class.          
            XmlSerializer xmlSerializer = new XmlSerializer(YourClassObject.GetType());
            // Creates a stream whose backing store is memory. 
            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializer.Serialize(xmlStream, YourClassObject);
                xmlStream.Position = 0;
                //Loads the XML document from the specified string.
                xmlDoc.Load(xmlStream);
               // return xmlDoc.InnerXml;
            }

            return   RemoveAllNamespaces(xmlDoc.InnerXml);
        }

        public static string RemoveAllNamespaces(string xmlDocument)
        {
            XElement xmlDocumentWithoutNs = RemoveAllNamespaces(XElement.Parse(xmlDocument));

            return xmlDocumentWithoutNs.ToString();
        }

        //Core recursion function
        private static XElement RemoveAllNamespaces(XElement xmlDocument)
        {
            if (!xmlDocument.HasElements)
            {
                XElement xElement = new XElement(xmlDocument.Name.LocalName);
                xElement.Value = xmlDocument.Value;

                foreach (XAttribute attribute in xmlDocument.Attributes())
                    xElement.Add(attribute);

                return xElement;
            }
            return new XElement(xmlDocument.Name.LocalName, xmlDocument.Elements().Select(el => RemoveAllNamespaces(el)));
        }


    }
  
    public class Error
    {
        public string message { get; set; }
        public string error { get; set; }

    }


}
