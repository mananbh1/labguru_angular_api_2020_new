﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Labguru_Angular_API_2020.Models
{
    public class TokenManager
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
