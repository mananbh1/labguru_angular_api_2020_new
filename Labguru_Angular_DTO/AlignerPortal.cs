﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Labguru_Angular_DTO
{
    
         public class SubmitCase_Insert
        {
            public Aligner_Basic_Information objAligner_Basic_Information { get; set; }
        }

        public class Aligner_Basic_Information
        {
            public string FirstName { get; set; }
            public string Age { get; set; }
            public string DeliveryDate { get; set; }
            public int PatientTypeID { get; set; }
            public string OutstationDate { get; set; }
            public string LastName { get; set; }
            public string Gender { get; set; }
            public int DeliverSessionID { get; set; }
            public int ArchCollectionID { get; set; }
            public int JobTypeID { get; set; }
            public int ArchID { get; set; }
            public string DentistEnclosedWithID { get; set; }
            public int CloseAllSpacesID { get; set; }
            public int? DigitalDentistID { get; set; }
            public string SpaceGainingPreferenceID { get; set; }
            public int? OverjetID { get; set; }
            public int? OverbiteID { get; set; } 
            public int? MidlineID { get; set; }   
            public int LoginRoleID { get; set; }   
            public int LoginTypeID { get; set; }   
            public int LoginUserID { get; set; }   
            public int StatusID { get; set; }   
            public int FolderID { get; set; }   
            public string LoginUserDisplayName { get; set; }   
            public string Remarks { get; set; }   
            public bool Attachment { get; set; }           
            public bool crown_upper_left_1 { get; set; }           
            public bool crown_upper_left_2 { get; set; }
            public bool crown_upper_left_3 { get; set; }
            public bool crown_upper_left_4 { get; set; }
            public bool crown_upper_left_5{ get; set; }
            public bool crown_upper_left_6 { get; set; }
            public bool crown_upper_left_7 { get; set; }
            public bool crown_upper_left_8 { get; set; }
            public bool crown_upper_right_8 { get; set; }
            public bool crown_upper_right_7 { get; set; }
            public bool crown_upper_right_6 { get; set; }
            public bool crown_upper_right_5 { get; set; }
            public bool crown_upper_right_4 { get; set; }
            public bool crown_upper_right_3 { get; set; }
            public bool crown_upper_right_2 { get; set; }
            public bool crown_upper_right_1 { get; set; }
            public bool crown_lower_left_8 { get; set; }
            public bool crown_lower_left_7 { get; set; }
            public bool crown_lower_left_6 { get; set; }
            public bool crown_lower_left_5 { get; set; }
            public bool crown_lower_left_4 { get; set; }
            public bool crown_lower_left_3 { get; set; }
            public bool crown_lower_left_2 { get; set; }
            public bool crown_lower_left_1 { get; set; }
            public bool crown_lower_right_8 { get; set; }
            public bool crown_lower_right_7 { get; set; }
            public bool crown_lower_right_6 { get; set; }
            public bool crown_lower_right_5 { get; set; }
            public bool crown_lower_right_4 { get; set; }
            public bool crown_lower_right_3 { get; set; }
            public bool crown_lower_right_2 { get; set; }
            public bool crown_lower_right_1 { get; set; }
            public bool implant_upper_left_8 { get; set; }
            public bool implant_upper_left_7 { get; set; }
            public bool implant_upper_left_6 { get; set; }
            public bool implant_upper_left_5 { get; set; }
            public bool implant_upper_left_4 { get; set; }
            public bool implant_upper_left_3 { get; set; }
            public bool implant_upper_left_2 { get; set; }
            public bool implant_upper_left_1 { get; set; }
            public bool implant_upper_right_8 { get; set; }
            public bool implant_upper_right_7 { get; set; }
            public bool implant_upper_right_6 { get; set; }
            public bool implant_upper_right_5 { get; set; }
            public bool implant_upper_right_4 { get; set; }
            public bool implant_upper_right_3 { get; set; }
            public bool implant_upper_right_2{ get; set; }
            public bool implant_upper_right_1 { get; set; }
            public bool implant_lower_left_8 { get; set; }
            public bool implant_lower_left_7 { get; set; }
            public bool implant_lower_left_6 { get; set; }
            public bool implant_lower_left_5 { get; set; }
            public bool implant_lower_left_4 { get; set; }
            public bool implant_lower_left_3 { get; set; }
            public bool implant_lower_left_2 { get; set; }
            public bool implant_lower_left_1 { get; set; }
            public bool implant_lower_right_8 { get; set; }
            public bool implant_lower_right_7 { get; set; }
            public bool implant_lower_right_6 { get; set; }
            public bool implant_lower_right_5 { get; set; }
            public bool implant_lower_right_4 { get; set; }
            public bool implant_lower_right_3 { get; set; }
            public bool implant_lower_right_2 { get; set; }
            public bool implant_lower_right_1 { get; set; }
            public string ProfileImageUrl { get; set; }
            public string FullFaceWithSmileURL { get; set; }
            public string FullFaceWithOutSmileURL { get; set; }
            public string OcclusalViewUpperURL { get; set; }
            public string OcclusalViewLowerURL { get; set; }
            public string LateralViewRightURL { get; set; }
            public string LateralViewLeftURL { get; set; }
            public string FrontViewURL { get; set; }
            public string OtherMediaURL { get; set; }
            public string RxFormsID { get; set; }
            public string PatientID { get; set; }
            public string PatientReferenceID { get; set; }
    }

    public class AlignerRX_Insert_Update_Output
    {
        public int status { get; set; }
        public string message { get; set; }

    }

    public class GetRXFormID
    {
        public int RxFormsID { get; set; }

    }


    public class AlignerRXForm_Data
    {
        public string Result { get; set; }
      
    }

    public class AlignerDashboard_status
    {
        public int NoOfRx { get; set; }        
        public string Status { get; set; }

    }



}

