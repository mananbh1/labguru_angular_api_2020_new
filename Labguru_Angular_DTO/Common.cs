﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Labguru_Angular_DTO
{
    public class CommonOutPut
    {

        public int id { get; set; }
        public string name { get; set; }


    }
    public class CommonList_Input
    {
        public int LoginCompanyID { get; set; } = 1;
        public int LoginRoleID { get; set; } = 1;
        public int LoginUserID { get; set; } = 1;
        public int LoginReferenceID { get; set; } = 417;
        public int FinancialYearID { get; set; } = 1;
        public int OrganizationUnitID { get; set; } = 7;
        public int LoginRoleTypeID { get; set; } = 2;
        public int SSCID { get; set; } = 3036;
        public string Keyword { get; set; } = "";
        public int CustomerBranchID { get; set; } = 0;

    }
    public class DDLMaster
    {
        public string DDLData { get; set; }

    }
    public class CommonList_NS_Input
    {
        public int LoginCompanyID { get; set; }
        public int OrganizationUnitID { get; set; }
        public int SSCID { get; set; }
    }

    public class Common_Error
    {

        public string Message { get; set; }


    }

    public class CommonValidationInput
    {

        public string ImpressionNo { get; set; }
        public string TransactionNo { get; set; }
        public int SituationID { get; set; }
        public int StatusID { get; set; }
        public int UserID { get; set; }
        public int ProductID { get; set; }

    }


    public class CommonValidationOutput
    {
        public string Message { get; set; }
    }

    public class Output_Data
    {
        public int status { get; set; }

        public string message { get; set; }

        public JToken data { get; set; }

        //public string FireBaseTransaction { get; set; }
    }
   
    public class Document_FolderPath
    {
        public string PhysicalFilePath { get; set; }
        public string VirtualPath { get; set; }
        public string ContainerName { get; set; }
        public string PhysicalFilePath_New { get; set; }
        public string FolderPath { get; set; }
        public int Documents_location { get; set; }
        public string AzurePath { get; set; }
        public string StorageConnectionString { get; set; }
    }

}
