﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

//

namespace Labguru_Angular_DTO
{
    public class JobEntry_Implant_Info_Input
    {

        public int JobEntryID { get; set; }
        public int JobEntryReferenceID { get; set; }
    }
    public class JobEntry_Implant_Info_OutPut
    {

        public Boolean Select { get; set; }
        public int ImplantCompanyDetailID { get; set; }
        public int ImplantCompanyID { get; set; }
        public string Code { get; set; }

        public string ImplantCompany { get; set; }

        public string Size { get; set; }
        public string Platform { get; set; }
        public decimal Quantity { get; set; }


    }
    public class JobEntry_Implant_Company
    {

        public Boolean Select { get; set; }

        public int ImplantCompanyID { get; set; }
        public string Code { get; set; }

        public string ImplantCompany { get; set; }

        public List<JobEntry_Implant_Company_Detail> Child { get; set; }

    }
    public class JobEntry_Implant_Company_Detail
    {
        public Boolean Select { get; set; }
        public int ImplantCompanyID { get; set; }
        public int ImplantCompanyDetailID { get; set; }
        public string Size { get; set; }
        public string Platform { get; set; }
        public decimal Quantity { get; set; }
    }
    public class JobEntry_Rework_Info_Input
    {
        public int OrganizationUnitID { get; set; } = 0;
        public string TransactionNumber { get; set; } = "";
        public int LoginUserID { get; set; } = 1;
        public int JobTypeID { get; set; } = 0;
        public int ModuleID { get; set; } = 0;
        public int ModuleReferenceID { get; set; } = 0;
        public int LoginReferenceID { get; set; } = 0;
    }
    public class Common_Validation_Output
    {


        public string Message { get; set; }
        public int JobEntryID { get; set; }
        public int JobEntryReferenceID { get; set; }
    }
    public class Common_Validation_Message
    {


        public string Message { get; set; }
    }
    public class JobEntry_Product_Price_Input
    {
        public int LoginUserID { get; set; }
        public int CustomeBranchID { get; set; }
        public int ProductID { get; set; }
        public DateTime Transactiondate { get; set; }
    }
    public class JobEntry_Product_Price_Output
    {
        public decimal Price { get; set; }
        public Boolean AllowPriceChange { get; set; }
        public string Alloy { get; set; }
    }
    public class JobEntry_Rework_Header
    {
        public string Result { get; set; }
        // public int ReferenceJobEntryID { get; set; }
        // public int ReferenceJobEntryReferenceID { get; set; }
        // public int CustomerBranchID { get; set; }

        // public string CustomerCode { get; set; }
        // public int DoctorID { get; set; }
        // public string Patient { get; set; }
        // public int CaseTypeID { get; set; }
        // public int StepID { get; set; }
        // public int FabricationModeID { get; set; }
        // public int PatientEntryID { get; set; }
        // public int PatientReferenceID { get; set; }
        // public string PatientEntryNo { get; set; }
        // public int? Age { get; set; }
        // public string Gender { get; set; }
        // public int VirtualStepID { get; set; }
        // public bool chargeRedoCase { get; set; }
        // public int chargeRedoCasePer { get; set; }

        //public  List<JobEntry_Rework_Detail> JobEntry_Rework_Detail { get; set; }
    }
    public class JobEntry_Rework_Detail
    {
        //  public string Result { get; set; }
        public bool edit { get; set; }
        public bool delete { get; set; }
        public string Product { get; set; }
        public string JobDesign { get; set; }
        public int NoOfUnit { get; set; }
        public string SelectedToothNumber { get; set; }
        public decimal Weight { get; set; }
        public decimal Price { get; set; }
        public decimal PriceListPrice { get; set; }
        public decimal RefPrice { get; set; }
        public decimal RefPriceListPrice { get; set; }
        public string Remark { get; set; }
        public string Createdby { get; set; }
        public string CreationDate { get; set; }
        public string ModifyBy { get; set; }
        public string ModificationDate { get; set; }
        public int UpperAligner { get; set; }
        public int LowerAligner { get; set; }
        public bool IsProductChange { get; set; }
        public bool IsSystemAdded { get; set; }
        public bool IsDigitalModelProduct { get; set; }
        public int ProductID { get; set; }
        public int JobDesignID { get; set; }
        public int JobEntryID { get; set; }
        public int JobEntryDetailID { get; set; }
        public int ModuleReferenceID { get; set; }

        public string DueDate { get; set; }
        public string Duetime { get; set; }
        public string Stage { get; set; }
        public string Alloy { get; set; }
        public int StageID { get; set; }
        public int AlloyID { get; set; }

    }
    public class JobEntry_Basic_Information
    {
        public int LoginUserID { get; set; }
        public int LoginReferenceID { get; set; }
        public int JobEntryID { get; set; }
        public int JobEntryReferenceID { get; set; }
        public int RxTypeID { get; set; }
        public int OrganizationUnitID { get; set; }
        public int JobEntryLocationID { get; set; }
        public int NumberingsystemDetailID { get; set; }
        public string Transactionnumber { get; set; }
        public int JobTypeSoftwareSubComponentID { get; set; }
        public int CaseTypeID { get; set; }
        public string Transactiondate { get; set; }
        public string EntryDate { get; set; }
        public int? DigitalDentistID { get; set; }
        public int chargeRedoCaseID { get; set; }
        public int? DB_EmployeeID { get; set; }
        public int? ThirdPartylabID { get; set; }
        public JobEntry_Redo objJobEntry_Redo { get; set; }
        public JobEntry_Correction objJobEntry_Correction { get; set; }
    }
    public class JobEntry_Redo
    {
        public int ReferenceJobEntryID { get; set; }
        public int ReferenceJobEntryReferenceID { get; set; }
        public int? LabRedoReasonID { get; set; }
        public int? DoctorRedoReasonID { get; set; }
        public byte? RedoAttempID { get; set; }
        public int? RedoReasionID { get; set; }
    }
    public class JobEntry_Correction
    {
        public int ReferenceJobEntryID { get; set; }
        public int ReferenceJobEntryReferenceID { get; set; }
        public int? CorrectionReasionID { get; set; }
        public int? CorrectionTypeID { get; set; }
    }

    public class Customer
    {
        public string name { get; set; }
        public int id { get; set; }
    }
    public class JobEntry_Customer_Information
    {
        public int CustomeBranchID { get; set; }
        public int DoctorID { get; set; }
        public string Patient { get; set; }
        public string Gender { get; set; }
        public string ReferenceEntryNo { get; set; }
        public string DueDate { get; set; }
        public string DueTime { get; set; }
        public int? UrgentCaseID { get; set; }




        //aligner fields--------------------

        public int PatientEntryID { get; set; }

        public int PatientReferenceID { get; set; }

        public int StepID { get; set; }

        public int FabricationModeID { get; set; }

        public int VirtualStepID { get; set; }

        public string txtVirtualStep { get; set; }
        public int CaseActionID { get; set; }
        public string DueDate_new { get; set; }
        public int PlanTypeID { get; set; }

        //misceleneous...
        public string PreRxNo { get; set; }
        public string CaseEntryNo { get; set; }

        public JobEntry_Customer_Information_EnclosedWith objJobEntry_Customer_Information_EnclosedWith { get; set; }

        public BadImpression objJobEntry_BadImpression { get; set; }
        // public Customer Customer { get; set; }

        public JobEntry_Customer_Information_Implant objJobEntry_Customer_Information_Implant { get; set; }
    }
    public class BadImpression
    {
        public bool BadJobEntry { get; set; }
        public int? BadReasionID { get; set; }
        public bool chkRubberBase { get; set; }
        public bool chkAlginate { get; set; }
    }
    public class finalArrayOfTooth
    {
        public int? ToothDetailID { get; set; } = 0;
        public string toothNumber { get; set; }
        public bool isPSelected { get; set; }
    }
    public class JobEntry_Product_Information
    {

        public bool IsDeleted { get; set; }
        public int NoOfUnit { get; set; }
        public string[] SelectedToothNumber { get; set; }

        public List<finalArrayOfTooth> finalArrayOfTooth { get; set; }
        public decimal Weight { get; set; }
        public decimal Price { get; set; }
        public decimal PriceListPrice { get; set; }
        public decimal RefPrice { get; set; }
        public decimal RefPriceListPrice { get; set; }
        public string Remark { get; set; }

        public int? UpperAligner { get; set; }
        public int? LowerAligner { get; set; }
        public bool IsProductChange { get; set; }
        public bool IsSystemAdded { get; set; }
        public bool IsDigitalModelProduct { get; set; }
        public int ProductID { get; set; }
        public int JobDesignID { get; set; }
        public int JobEntryID { get; set; }
        public int JobEntryDetailID { get; set; }
        public int ModuleReferenceID { get; set; }
        public string DueDate { get; set; }
        public string Duetime { get; set; }
        public string Stage { get; set; }
        public int JobStageID { get; set; }
        public string Alloy { get; set; }
        public int? AlloyID { get; set; }
        public int? ShadeID { get; set; }
        public int? JobEntryDesignDetailID { get; set; }
        public int? PJobStageID { get; set; }




    }
    public class JobEntry_Customer_Information_EnclosedWith
    {
        //public string Key { get; set; }
        //public string Value { get; set; }

        public bool ImpressionUL { get; set; }
        public bool ImpressionLower { get; set; }
        public bool chkModelsUL { get; set; }  //ModuleUL
        public bool ModelsLower { get; set; }
        public bool FullArch { get; set; }
        public string FullArch_Qty { get; set; }
        public bool HalfArch { get; set; }
        public string HalfArch_Qty { get; set; }
        public bool TTFullArch { get; set; }
        public string TTFullArch_Qty { get; set; }
        public bool TTHalfArch { get; set; }
        public string TTHalfArch_Qty { get; set; }
        public bool Bite { get; set; }
        public bool StudyModel { get; set; }
        public bool PD { get; set; }
        public bool Articulator { get; set; }
        public string Articulator_Qty { get; set; }
        public bool Photos { get; set; }
        public bool Photos_Email { get; set; }
        public bool Photos_Case { get; set; }
        public bool Photos_Rx { get; set; }

    }
    public class JobEntry_Customer_Information_Implant_Info
    {
        public int ImplantCompanyDetailID { get; set; }
        public int ImplantCompanyID { get; set; }
        public string Code { get; set; }
        public string ImplantCompany { get; set; }
        public string Size { get; set; }
        public string Platform { get; set; }
        public int Quantity { get; set; }

        public int implantIndex { get; set; }
        public int subImplantIndex { get; set; }

    }
    public class JobEntry_Customer_Information_Implant
    {

        public string txtImpressionPost { get; set; }
        public string txtLabAnalog { get; set; }
        public string txtImplant { get; set; }

        public List<JobEntry_Customer_Information_Implant_Info> objJobEntry_Customer_Information_Implant_Info { get; set; }


    }
    public class JobEntry_Preference
    {
        //Prep Design
        public bool Shoulder { get; set; }
        public bool GumsMargin { get; set; }
        public bool DeepMargin { get; set; }
        public bool DeepChamper { get; set; }
        public bool LightChamfer { get; set; }
        public bool ShoulderWithBevel { get; set; }
        public bool FeatherEdge { get; set; }
        public bool Chamfer { get; set; }

        //Staining

        public bool Light { get; set; }
        public bool Medium { get; set; }
        public bool Heavy { get; set; }
        public bool Dark { get; set; }
        public bool Staining_Pan { get; set; }
        public bool Tobacco { get; set; }
        public bool Florocis { get; set; }

        //Occlusal Contact
        public bool FollRelief { get; set; }
        public bool PositiveContact { get; set; }
        public bool Occlusal_Others { get; set; }

        //If No Occlusal clearance
        public bool MetalOcculusal { get; set; }
        public bool ReductionCoping { get; set; }
        public bool AdjustOpposite { get; set; }
        public bool MetalIsland { get; set; }

        //Contacts and Embrasures

        public bool Broad_CE { get; set; }
        public bool Light_CE { get; set; }
        public bool Tight_CE { get; set; }
        public bool CloseEmbrasures { get; set; }
        public bool DiastemaCloseMesial { get; set; }
        public bool CloseDistal { get; set; }
        public bool NoHighPoint { get; set; }

        //Ceramic Margin
        public bool Facial180 { get; set; }
        public bool AllAround360 { get; set; }

        //Pontic Design
        public bool ModifiedRidge { get; set; }
        public bool FullRidge { get; set; }
        public bool Sanitary { get; set; }
        public bool Ovate { get; set; }
        public bool Bullet { get; set; }


        //Collar and Metal Design
        public bool NoMetalCollar { get; set; }
        public bool MetalLingualCollar { get; set; }
        public bool MetalCollar360 { get; set; }
        public bool MetalOccusal_Lingual { get; set; }
        public bool MetalCollar180 { get; set; }
        public bool pointMetalColor { get; set; }

        //Special Instructions

        public bool ShoulderPorcelain { get; set; }
        public bool PinkPorcelain { get; set; }

        public bool BiteRaise5mm { get; set; }
        public bool BiteRaise6mm { get; set; }
        public bool BiteRaise7mm { get; set; }

        //------------------

        public string OtherInstructions { get; set; }

        public int AttentionID { get; set; }
    }
    public class JobEntry_Insert_Update
    {
        public JobEntry_Basic_Information objJobEntry_Basic_Information { get; set; }
        public JobEntry_Customer_Information objJobEntry_Customer_Information { get; set; }
        public List<JobEntry_Product_Information> objJobEntry_Product_Information { get; set; }
        public JobEntry_Preference objJobEntry_Preference { get; set; }

    }

    public class JobEntry_Insert_Update_Output
    {
        public int status { get; set; }
        public string message { get; set; }

    }
    public class JobEntry_Search
    {
        public string TransactionNumber { get; set; } = "";
        public string JobEntryID { get; set; }
        public string ReferenceID { get; set; }
        public string ProductID { get; set; }
        public string CustomerID { get; set; }
        public int LoginUserID { get; set; } = 1;
    }

    public class JobEntry_Search_Output
    {
        public string Result { get; set; } = "";

    }
    public class JobEntry_AppliedCoupons
    {
        //   public int AppliedCouponID { get; set; }
        //  public int JobEntryID { get; set; }
        //   public int JobEntryReferenceID { get; set; }
        public string JobEntryNo { get; set; }
        //   public int ProductID { get; set; }
        // public string ProductCode { get; set; }
        public string Product { get; set; }
        //   public int JobDesignID { get; set; }
        // public string JobDesignCode { get; set; }
        public string JobDesign { get; set; }
        public int SelectedTeeth { get; set; }
        // public string PerUnits { get; set; }
        // public int CouponID { get; set; }
        // public int CouponDetailID { get; set; }
        public string Coupon { get; set; }
        // public decimal Price { get; set; }
        // public decimal Discount { get; set; }
        //  public decimal FlatAmount { get; set; }
        public string Remark { get; set; }
        //public decimal Rate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime ModificationDate { get; set; }
        // public int TypeID { get; set; }
        // public string OldCoupon { get; set; }
    }
    public class JobEntry_SupplierWarranty
    {
        public int JobEntryID { get; set; }
        public int JobEntryReferenceID { get; set; }
        public string JobEntryNo { get; set; }
        public int ProductID { get; set; }
        public string ProductCode { get; set; }
        public string Product { get; set; }
        public string WarrantyCardNo { get; set; }
        public string Remark { get; set; }
        public DateTime ModificationDate { get; set; }
        public string ModifyBy { get; set; }
    }
    public class JobEntry_Print
    {
        public string Contents { get; set; }
        public string JobEntryNo { get; set; }
        public byte[] Barcode { get; set; }
        public string ImagePath { get; set; }
        public string SecondPage { get; set; }
        public string URL { get; set; }

        public string FolderPath { get; set; }

        public string CompletePath { get; set; }

        public string TempURL { get; set; }

        public string CompleteVirtualPath { get; set; }


    }


}
