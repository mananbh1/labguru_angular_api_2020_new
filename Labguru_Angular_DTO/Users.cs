﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Labguru_Angular_DTO
{
    [Table("Admin_MUser", Schema = "dbo")]


    public class Login_Input
    {
        [Key]

        public string Code { get; set; }
        public string Password { get; set; }
        public string IP { get; set; }


    }

    public class Login_Output
    {
        [Key]
        public int LoginReferenceID { get; set; } = 417;
        public int LoginUserID { get; set; }
        public string Code { get; set; }
        public string LoginUser { get; set; }
        public int LoginCompanyID { get; set; }
        public string LoginCompany { get; set; }
        public int LoginRoleID { get; set; }
        public string LoginRole { get; set; }
        public string ValidationMsg { get; set; }
        public string Profile { get; set; }
        public int FinancialYearID { get; set; }
        public string FinancialYear { get; set; }
        public int NatureOfBusinessID { get; set; }

        public string MobileNumber { get; set; }
        public string EmailID { get; set; }

    }

    public class UserMenu
    {
        public string MenuName { get; set; }
        public string Icon { get; set; }
        public string Collapse { get; set; }
        public string Route { get; set; }
        public List<Menu> ChildMenu { get; set; }
    }


    public class Menu
    {
        public int MenuID { get; set; }
        public string MenuName { get; set; }
        public int ParentMenuID { get; set; }
        //public string Icon { get; set; }
        //public string Collapse { get; set; }
        //public string Route { get; set; }
    }

    public class UserProfile_Input
    {
        public string Code { get; set; } = "";
        public string OldPassword { get; set; } = "";
        public string NewPassword { get; set; } = "";
        public string LoginUser { get; set; } = "";
        public string MobileNumber { get; set; } = "";
        public string EmailID { get; set; } = "";
        public string Profile { get; set; } = "";
    }
    public class UserProfile_Output
    {
        public string message { get; set; }
        public int status { get; set; }
    }

}



