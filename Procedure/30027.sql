insert into Auto_SoftwareSubcomponent([SoftwareSubComponentID],[LanguageID],[SoftwareComponentID],[Description],[Description_Display],[IsUDTHead],
[NumberingTypeApplicable],[IsReport],[ScopeLevelID],[InterfacetypeID],[HasSpecialPrivilege],[Abbreviation],[HasSpecialPrivilegeOnTheSamePage],
[Available],[ParentSoftwareSubComponentID],[ReportGroupID],[NumberingSystemSoftwareSubComponentID],[ShowAbbreviation],[IsReversal],[IsUDT],[PSSCID],
[RoutingPath],[DBObjectName],[IsVisible],[menusequence],[MenuIcon],[DDLMasterID]) 
values(30027,1,30,'Submit Case','Submit Case',0,0,0,1,1,0,'SC',0,1,30024,NULL,1,1,0,NULL,NULL,'Reports/commonreport/Submit Case/30027/0','uspRptSubmitCase_GR',1,'3.8','keyboard_arrow_right',NULL)
go
insert into Auto_MenuItem([MenuItemID],[LanguageID],[SoftwareSubComponentID],[ParentMenuItemID],[Description],[Url],[SequenceNo],[ApplicationID],
[ShowMenu],[MainParentMenuItemID],[InformationTypeID]) values(1307,1,30027,1224,'Submit Case','',25,1,1,0,1)

go

insert Auto_Admin_InterfacePrivilege
select 30027,5
go 
insert Auto_MenuItem_RoleType_Mapping
select 1, 30027
go
insert Auto_MenuItem_RoleType_Mapping
select 2, 30027
go

insert Admin_MRolePrivilege 
select 1,InterfaceprivilegeID,0,1,GETDATE() from Auto_Admin_InterfacePrivilege 
where InterfaceprivilegeID  not in 
(select  InterfaceprivilegeID from Admin_MRolePrivilege)

go

insert Admin_MRolePrivilege 
select dbo.get_supperAdmin_RoleID(),InterfaceprivilegeID,0,1,GETDATE() from Auto_Admin_InterfacePrivilege 
where InterfaceprivilegeID  not in 
(select  InterfaceprivilegeID from Admin_MRolePrivilege where roleid = dbo.get_supperAdmin_RoleID())
