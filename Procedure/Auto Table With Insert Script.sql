if exists (select 1 from sys.tables where name = 'Auto_SP_PatientType')
	drop table Auto_SP_PatientType
go
create table Auto_SP_PatientType
(
	PatientTypeID tinyint,
	Description varchar(100)
)
go
Insert into Auto_SP_PatientType values (1,'Local')
Insert into Auto_SP_PatientType values (2,'Outsation')
go
if exists (select 1 from sys.tables where name = 'Auto_SP_DeliverSession')
	drop table Auto_SP_DeliverSession
go
create table Auto_SP_DeliverSession
(
	DeliverSessionID tinyint,
	Description varchar(100)
)
go
Insert into Auto_SP_DeliverSession values (1,'Morning 10am to 2 pm')
Insert into Auto_SP_DeliverSession values (2,'Evening 6pm to 10 pm')
go
if exists (select 1 from sys.tables where name = 'Auto_SP_ArchCollection')
	drop table Auto_SP_ArchCollection
go
create table Auto_SP_ArchCollection
(
	ArchCollectionID tinyint,
	Description varchar(100)
)
go
Insert into Auto_SP_ArchCollection values (1,'Anterior')
Insert into Auto_SP_ArchCollection values (2,'Full')
go
if exists (select 1 from sys.tables where name = 'Auto_SP_JobType')
	drop table Auto_SP_JobType
go
create table Auto_SP_JobType
(
	JobTypeID tinyint,
	Description varchar(100)
)
go
Insert into Auto_SP_JobType values (1,'New')
Insert into Auto_SP_JobType values (2,'Mid Course Correction')
Insert into Auto_SP_JobType values (3,'Refinement')
go
if exists (select 1 from sys.tables where name = 'Auto_SP_DentistEnclosedWith')
	drop table Auto_SP_DentistEnclosedWith
go
create table Auto_SP_DentistEnclosedWith
(
	DentistEnclosedWithID tinyint,
	Description varchar(100)
)
go
Insert into Auto_SP_DentistEnclosedWith values (1,'Diagnostic Imp Upper')
Insert into Auto_SP_DentistEnclosedWith values (2,'Diagnostic Imp Lower')
Insert into Auto_SP_DentistEnclosedWith values (3,'Diagnostic Model Upper')
Insert into Auto_SP_DentistEnclosedWith values (4,'Diagnostic Model Lower')
Insert into Auto_SP_DentistEnclosedWith values (5,'Final Imp Upper')
Insert into Auto_SP_DentistEnclosedWith values (6,'Final Imp Lower')
Insert into Auto_SP_DentistEnclosedWith values (7,'Final Modal Upper') 
Insert into Auto_SP_DentistEnclosedWith values (8,'Aluwax Bite')
go
if exists (select 1 from sys.tables where name = 'Auto_SP_Arch')
	drop table Auto_SP_Arch
go
create table Auto_SP_Arch
(
	ArchID tinyint,
	Description varchar(100)
)
go
Insert into Auto_SP_Arch values (1,'Single Arch Upper')
Insert into Auto_SP_Arch values (2,'Single Arch Lower')
Insert into Auto_SP_Arch values (3,'Both Arches')
go
if exists (select 1 from sys.tables where name = 'Auto_SP_FaceGainingPrefrence')
	drop table Auto_SP_FaceGainingPrefrence
go
create table Auto_SP_FaceGainingPrefrence
(
	FaceGainingPrefrenceID tinyint,
	Description varchar(100)
)
go
Insert into Auto_SP_FaceGainingPrefrence values (1,'IPR')
Insert into Auto_SP_FaceGainingPrefrence values (2,'Extraction')
Insert into Auto_SP_FaceGainingPrefrence values (3,'Expansion')
go
if exists (select 1 from sys.tables where name = 'Auto_SP_Overjet')
	drop table Auto_SP_Overjet
go
create table Auto_SP_Overjet
(
	OverjetID tinyint,
	Description varchar(100)
)
go
Insert into Auto_SP_Overjet values (1,'Maintain')
Insert into Auto_SP_Overjet values (2,'Increase')
Insert into Auto_SP_Overjet values (3,'Decrease')
go
if exists (select 1 from sys.tables where name = 'Auto_SP_Overbite')
	drop table Auto_SP_Overbite
go
create table Auto_SP_Overbite
(
	OverbiteID tinyint,
	Description varchar(100)
)
go
Insert into Auto_SP_Overbite values (1,'Maintain')
Insert into Auto_SP_Overbite values (2,'Increase')
Insert into Auto_SP_Overbite values (3,'Decrease')
go
if exists (select 1 from sys.tables where name = 'Auto_SP_CloseAllspaces')
	drop table Auto_SP_CloseAllspaces
go
create table Auto_SP_CloseAllspaces
(
	CloseAllspacesID tinyint,
	Description varchar(100)
)
go
Insert into Auto_SP_CloseAllspaces values (1,'Maintain Diestem')
Insert into Auto_SP_CloseAllspaces values (2,'Leave Space Diastem')
Insert into Auto_SP_CloseAllspaces values (3,'Close All Spaces')
go
if exists (select 1 from sys.tables where name = 'Auto_SP_Midline')
	drop table Auto_SP_Midline
go
create table Auto_SP_Midline
(
	MidlineID tinyint,
	Description varchar(100)
)
go
Insert into Auto_SP_Midline values (1,'Maintain')
Insert into Auto_SP_Midline values (2,'Improve')