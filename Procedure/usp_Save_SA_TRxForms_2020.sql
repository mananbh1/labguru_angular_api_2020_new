--alter table SA_TRxForms alter column DentistEnclosedWithID varchar(100)
--alter table SA_TRxForms alter column SpaceGainingPreferenceID varchar(100)

create procedure usp_Save_SA_TRxForms_2020
(
	@ijsondata varchar(max)
)
as
/*
usp_Save_SA_TRxForms '{
  "LoginTypeID": "2",
  "LoginRoleID": "1",
  "LoginUserID": "1",
  "LoginUserDisplayName": "Admin",
  "RxFormsID": "0",
  "CustomerID": "0",
  "FirstName": "Manan",
  "Age": "25",
  "DeliveryDate": "2020-06-17T18:30:00.000Z",
  "PatientTypeID": 1,
  "OutstationDate": "2020-06-17T18:30:00.000Z",
  "LastName": "Bhavsar",
  "Gender": 1,
  "DeliverSessionID": 1,
  "ArchCollectionID": 1,
  "Attachment": true,
  "SpaceGainingPreferenceID": "1,2,3",
  "DentistEnclosedWithID": "1,2,3,4",
  "JobTypeID": 1,
  "ArchID": 2,
  "OverbiteID": 1,
  "MidlineID": 2,
  "OverjetID": 1,
  "Remarks": "Testing",
  "ProfileImageUrl": "Capture.PNG",
  "FullFaceWithSmileURL": "Capture.PNG",
  "FullFaceWithOutSmileURL": "download (4).jpg",
  "OcclusalViewUpperURL": "Capture.PNG",
  "OcclusalViewLowerURL": "download (4).jpg",
  "LateralViewRightURL": "download (4).jpg",
  "LateralViewLeftURL": "download (4).jpg",
  "FrontViewURL": "download (4).jpg",
  "OtherMediaURL": "download (4).jpg"
}'
*/
BEGIN --START THE PROCEDURE
	set nocount on
	Begin -- create temp table to store json data.  START

	create table #SA_TRxForms([ID] [int] IDENTITY(1,1) NOT NULL,
		[RxFormsID] [int], [CustomerPortalID] [int], [PatientID] [int], [PatientReferenceID] [int], [UpperArch] [int], [LowerArch] [int],
		[NoOfFullArchImp] [int], [StatusID] [tinyint], [JobTypeID] [tinyint], [OverjetID] [tinyint], [ArchID] [tinyint],
		[OverbiteID] [tinyint], [OutstationDate] [datetime], [DeliveryDate] [datetime], [DeliverSessionID] [tinyint],
		[DentistEnclosedWithID] [varchar](100), [MidlineID] [tinyint], [PatientTypeID] [tinyint], [ArchCollectionID] [tinyint],
		[SpaceGainingPreferenceID] [varchar](100), [CloseAllSpacesID] [tinyint], [ProfileImageUrl] [nvarchar](250), 
		[FullFaceWithSmileURL] [nvarchar](250), [FullFaceWithOutSmileURL] [nvarchar](250), [OcclusalViewUpperURL] [nvarchar](250),
		[OcclusalViewLowerURL] [nvarchar](250), [LateralViewRightURL] [nvarchar](250), [FrontViewURL] [nvarchar](250),
		[LateralViewLeftURL] [nvarchar](250), [OtherMediaURL] [nvarchar](250), [FilesUploadURL] [nvarchar](250),
		[Attachment] [bit], [PonticRequired] [bit], [leaveSpaceToDistalValue] [nvarchar](100), [Remarks] [varchar](500),
		[CreatedbyID] [int], [CreationDate] [datetime], [ModifyByID] [int], [ModificationDate] [datetime])
	End -- create temp table to store json data.  END
	Begin -- Variable Declarion START
		declare @dCURRENT_TIMESTAMP datetime = CURRENT_TIMESTAMP, @gRxFormsID int
	End -- Variable Declarion END
	Begin Try
		begin tran
			Begin -- Get declared (g) variable value from table / Json. START 
				select @gRxFormsID = JSON_VALUE(@ijsondata,'$.RxFormsID')

			End-- Get declared (g) variable value from table / Json. END
			Begin -- Read the JSON String and Store data in Temp Table START
				insert into #SA_TRxForms(RxFormsID, CustomerPortalID, PatientID, PatientReferenceID, UpperArch, LowerArch, NoOfFullArchImp, StatusID, 
					JobTypeID, OverjetID, ArchID, OverbiteID, OutstationDate, DeliveryDate, DeliverSessionID, DentistEnclosedWithID, MidlineID, 
					PatientTypeID, ArchCollectionID, SpaceGainingPreferenceID, CloseAllSpacesID, ProfileImageUrl, FullFaceWithSmileURL, 
					FullFaceWithOutSmileURL, OcclusalViewUpperURL, OcclusalViewLowerURL, LateralViewRightURL, FrontViewURL, LateralViewLeftURL, 
					OtherMediaURL, FilesUploadURL, Attachment, PonticRequired, leaveSpaceToDistalValue, Remarks, 
					CreatedbyID, CreationDate, ModifyByID, ModificationDate)
				select 
					JSON_VALUE(@ijsondata,'$.RxFormsID') as RxFormsID,
					JSON_VALUE(@ijsondata,'$.LoginUserID') as CustomerPortalID, 
					JSON_VALUE(@ijsondata,'$.PatientID') as PatientID, 
					JSON_VALUE(@ijsondata,'$.PatientReferenceID') as PatientReferenceID,
					JSON_VALUE(@ijsondata,'$.UpperArch') as UpperArch,
					JSON_VALUE(@ijsondata,'$.LowerArch') as LowerArch,
					JSON_VALUE(@ijsondata,'$.NoOfFullArchImp') as NoOfFullArchImp,
					JSON_VALUE(@ijsondata,'$.StatusID') as StatusID,
					JSON_VALUE(@ijsondata,'$.JobTypeID') as JobTypeID,
					JSON_VALUE(@ijsondata,'$.OverjetID') as OverjetID,
					JSON_VALUE(@ijsondata,'$.ArchID') as ArchID,
					JSON_VALUE(@ijsondata,'$.OverbiteID') as OverbiteID,
					JSON_VALUE(@ijsondata,'$.OutstationDate') as OutstationDate,
					JSON_VALUE(@ijsondata,'$.DeliveryDate') as DeliveryDate,
					JSON_VALUE(@ijsondata,'$.DeliverSessionID') as DeliverSessionID,
					JSON_VALUE(@ijsondata,'$.DentistEnclosedWithID') as DentistEnclosedWithID,
					JSON_VALUE(@ijsondata,'$.MidlineID') as MidlineID,
					JSON_VALUE(@ijsondata,'$.PatientTypeID') as PatientTypeID,
					JSON_VALUE(@ijsondata,'$.ArchCollectionID') as ArchCollectionID,
					JSON_VALUE(@ijsondata,'$.SpaceGainingPreferenceID') as SpaceGainingPreferenceID,
					JSON_VALUE(@ijsondata,'$.CloseAllSpacesID') as CloseAllSpacesID,
					JSON_VALUE(@ijsondata,'$.ProfileImageUrl') as ProfileImageUrl,
					JSON_VALUE(@ijsondata,'$.FullFaceWithSmileURL') as FullFaceWithSmileURL,
					JSON_VALUE(@ijsondata,'$.FullFaceWithOutSmileURL') as FullFaceWithOutSmileURL,
					JSON_VALUE(@ijsondata,'$.OcclusalViewUpperURL') as OcclusalViewUpperURL,
					JSON_VALUE(@ijsondata,'$.OcclusalViewLowerURL') as OcclusalViewLowerURL,
					JSON_VALUE(@ijsondata,'$.LateralViewRightURL') as LateralViewRightURL,
					JSON_VALUE(@ijsondata,'$.FrontViewURL') as FrontViewURL,
					JSON_VALUE(@ijsondata,'$.LateralViewLeftURL') as LateralViewLeftURL,
					JSON_VALUE(@ijsondata,'$.OtherMediaURL') as OtherMediaURL,
					JSON_VALUE(@ijsondata,'$.FilesUploadURL') as FilesUploadURL,
					JSON_VALUE(@ijsondata,'$.Attachment') as Attachment,
					JSON_VALUE(@ijsondata,'$.PonticRequired') as PonticRequired,
					JSON_VALUE(@ijsondata,'$.leaveSpaceToDistalValue') as leaveSpaceToDistalValue,
					JSON_VALUE(@ijsondata,'$.Remarks') as Remarks,
					JSON_VALUE(@ijsondata,'$.LoginUserID') as CreatedbyID,
					@dCURRENT_TIMESTAMP as CreationDate,
					JSON_VALUE(@ijsondata,'$.LoginUserID') as ModifyByID,
					@dCURRENT_TIMESTAMP as ModificationDate
			End -- Read the JSON String and Store data in Temp Table END
			Begin -- Start Data Insert into Main Tables START
				If (isnull(@gRxFormsID,0) = 0) 
					begin
						insert into SA_TRxForms(CustomerPortalID, PatientID, PatientReferenceID, UpperArch, LowerArch, NoOfFullArchImp, StatusID, JobTypeID,
							OverjetID, ArchID, OverbiteID, OutstationDate, DeliveryDate, DeliverSessionID, DentistEnclosedWithID, MidlineID, PatientTypeID,
							ArchCollectionID, SpaceGainingPreferenceID, CloseAllSpacesID, ProfileImageUrl, FullFaceWithSmileURL, FullFaceWithOutSmileURL,
							OcclusalViewUpperURL, OcclusalViewLowerURL, LateralViewRightURL, FrontViewURL, LateralViewLeftURL, OtherMediaURL, FilesUploadURL,
							Attachment, PonticRequired, leaveSpaceToDistalValue, Remarks, CreatedbyID, CreationDate, ModifyByID, ModificationDate)
						Select CustomerPortalID, PatientID, PatientReferenceID, UpperArch, LowerArch, NoOfFullArchImp, StatusID, JobTypeID,
							OverjetID, ArchID, OverbiteID, OutstationDate, DeliveryDate, DeliverSessionID, DentistEnclosedWithID, MidlineID, PatientTypeID,
							ArchCollectionID, SpaceGainingPreferenceID, CloseAllSpacesID, ProfileImageUrl, FullFaceWithSmileURL, FullFaceWithOutSmileURL,
							OcclusalViewUpperURL, OcclusalViewLowerURL, LateralViewRightURL, FrontViewURL, LateralViewLeftURL, OtherMediaURL, FilesUploadURL,
							Attachment, PonticRequired, leaveSpaceToDistalValue, Remarks, CreatedbyID, CreationDate, ModifyByID, ModificationDate
						From #SA_TRxForms

						set @gRxFormsID = @@IDENTITY
					end
				else
					begin
						update rxf
							set rxf.UpperArch = trxf.UpperArch
						from 
							SA_TRxForms rxf
						Inner join #SA_TRxForms trxf
						on rxf.RxFormsID = trxf.RxFormsID
					end
			End -- End Data Insert into Main Tables END

		Commit Tran
		select 
		200 as [status], 
		'Record Save Sucessfully With ID Number:' + cast(@gRxFormsID as varchar(10)) as [message] 
	End Try
	Begin Catch
		IF @@TRANCOUNT > 0 rollback tran
		select 
			409 as [status], 
			'An Error Occured While Saving the Record.: ' + error_message() as [message] 
	End Catch
	set nocount off	
END
