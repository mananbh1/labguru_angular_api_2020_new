create procedure [dbo].[usp_select_SA_TRxForms_2020]
(  
	@ijsondata		varchar(max),
	@SituationID	[int]=0
)
AS
BEGIN
	declare @gRxFormsID int = 1

	select @gRxFormsID   = JSON_VALUE(@ijsondata,'$.RxFormsID')

	select (
		select RxFormsID, CustomerPortalID, PatientID, PatientReferenceID, UpperArch, LowerArch, NoOfFullArchImp, StatusID, JobTypeID,
			OverjetID, ArchID, OverbiteID, OutstationDate, DeliveryDate, DeliverSessionID, DentistEnclosedWithID, MidlineID, PatientTypeID,
			ArchCollectionID, SpaceGainingPreferenceID, CloseAllSpacesID, ProfileImageUrl, FullFaceWithSmileURL, FullFaceWithOutSmileURL,
			OcclusalViewUpperURL, OcclusalViewLowerURL, LateralViewRightURL, FrontViewURL, LateralViewLeftURL, OtherMediaURL, FilesUploadURL,
			Attachment, PonticRequired, leaveSpaceToDistalValue, Remarks, CreatedbyID, CreationDate, ModifyByID, ModificationDate
		from 
			SA_TRxForms 
		where RxFormsID = @gRxFormsID
		FOR JSON PATH, WITHOUT_ARRAY_WRAPPER, INCLUDE_NULL_VALUES
	) as Result
END